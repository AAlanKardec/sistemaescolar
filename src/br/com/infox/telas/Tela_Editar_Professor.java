/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.infox.telas;

import br.com.infox.dal.ModoConexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.JOptionPane;

/**
 *
 * @author LabaAugustus
 */
public class Tela_Editar_Professor extends javax.swing.JFrame {

    Connection conexao = null;
    //criando variaveis especiais para conexao com o banco
    //PreparedStatement e ResultSet são frameworks do pacote java.sql
    //e serve para preparar e executar as isntruções SQL
    PreparedStatement pst = null;
    ResultSet rs = null;
    
    public Tela_Editar_Professor() {
        initComponents();
        conexao = ModoConexao.conector();
        
        codprofessor.setText(Tela_Professor.profId.getText());
        consultar();
    }
    
    private void alterar() {
        String sql = "update tbprofessor set nomeprof=?,enderecoprof=?, bairroprof=?, nascimentoprof=?,"
                + "cepprof=?,cidadeprof=?,estadoprof=?,emailprof=?,rgprof=?,cpfprof=?,telefoneprof=?,celularprof=?,"
                + "entradaprof=?,saidaprof=?,disciplinaprof=?,statusprof=? where idprof=?";
        try {
            pst = conexao.prepareStatement(sql);
            pst.setString(1, nomeprofessor.getText());
            pst.setString(2, endprofessor.getText());
            pst.setString(3, bairroprofessor.getText());
            pst.setString(4, datanascimentoprofessor.getText());
            pst.setString(5, cepprofessor.getText());
            pst.setString(6, cidadeprofessor.getText());
            pst.setString(7, estadoprofessor.getSelectedItem().toString());
            pst.setString(8, emailprofessor.getText());
            pst.setString(9, rgprofessor.getText());
            pst.setString(10, cpfprofessor.getText());
            pst.setString(11, telefoneprofessor.getText());
            pst.setString(12, celularprofessor.getText());
            pst.setString(13, entradaprofessor.getText());
            pst.setString(14, saidaprofessor.getText());
            pst.setString(15, disciplinaprofessor.getText());
            pst.setString(16, statusprofessor.getText());
            pst.setString(17, codprofessor.getText());

            if ((nomeprofessor.getText().isEmpty()) || (endprofessor.getText().isEmpty()) || (bairroprofessor.getText().isEmpty()) ||
                    (datanascimentoprofessor.getText().isEmpty()) || (cepprofessor.getText().isEmpty()) || 
                    (cidadeprofessor.getText().isEmpty()) || (emailprofessor.getText().isEmpty()) || 
                    (rgprofessor.getText().isEmpty()) || (cpfprofessor.getText().isEmpty()) || (telefoneprofessor.getText().isEmpty()) ||
                    (celularprofessor.getText().isEmpty()) || (entradaprofessor.getText().isEmpty()) ||
                    (saidaprofessor.getText().isEmpty()) || (disciplinaprofessor.getText().isEmpty()) || 
                    (statusprofessor.getText().isEmpty())) {
                JOptionPane.showMessageDialog(null, "Preencha todos os campos obrigatorios");
            } else {

                //a linha abaixo atualiza a tabela usuario com os dados do formulario
                //a estrutura abaixo é usada para confirmar a alteração dos dados na tabela   
                int adicionado = pst.executeUpdate();
                if (adicionado > 0) {
                    JOptionPane.showMessageDialog(null, "Usuario alterado com Sucesso");
                    codprofessor.setText(null);
                    nomeprofessor.setText(null);
                    endprofessor.setText(null);
                    bairroprofessor.setText(null);
                    datanascimentoprofessor.setText(null);
                    cepprofessor.setText(null);
                    cidadeprofessor.setText(null);
                    estadoprofessor.setSelectedItem(null);
                    emailprofessor.setText(null);
                    rgprofessor.setText(null);
                    cpfprofessor.setText(null);
                    telefoneprofessor.setText(null);
                    celularprofessor.setText(null);
                    entradaprofessor.setText(null);
                    saidaprofessor.setText(null);
                    disciplinaprofessor.setText(null);
                    statusprofessor.setText(null);

                }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }
    
    private void consultar() {
        String sql = "select * from tbprofessor where idprof=?";
        try {
            pst = conexao.prepareStatement(sql);
            pst.setString(1, codprofessor.getText());
            rs = pst.executeQuery();
            if (rs.next()) {
                codprofessor.setText(rs.getString(1));
                nomeprofessor.setText(rs.getString(2));
                endprofessor.setText(rs.getString(3));
                datanascimentoprofessor.setText(rs.getString(5));
                bairroprofessor.setText(rs.getString(4));
                cepprofessor.setText(rs.getString(6));
                cidadeprofessor.setText(rs.getString(7));
                estadoprofessor.setSelectedItem(rs.getString(8));
                emailprofessor.setText(rs.getString(9));
                rgprofessor.setText(rs.getString(10));
                cpfprofessor.setText(rs.getString(11));
                telefoneprofessor.setText(rs.getString(12));
                celularprofessor.setText(rs.getString(13));
                entradaprofessor.setText(rs.getString(14));
                saidaprofessor.setText(rs.getString(15));
                disciplinaprofessor.setText(rs.getString(16));
                statusprofessor.setText(rs.getString(17));
                // alinha abaixo se refere ao combobox
                
            } else {
                JOptionPane.showMessageDialog(null, "Usuario não cadastrado");
                // as linhas abaixo limpam os campos
               
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }
    
    
    
    
    
    
    
    
    
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        entradaprofessor = new javax.swing.JFormattedTextField();
        disciplinaprofessor = new javax.swing.JTextField();
        lbldicprof = new javax.swing.JLabel();
        lblcepprof = new javax.swing.JLabel();
        lblestadoprof = new javax.swing.JLabel();
        lblloginprof = new javax.swing.JLabel();
        endprofessor = new javax.swing.JTextField();
        lblsenhaprof = new javax.swing.JLabel();
        codprofessor = new javax.swing.JTextField();
        datanascimentoprofessor = new javax.swing.JFormattedTextField();
        estadoprofessor = new javax.swing.JComboBox<>();
        lblstatusprof = new javax.swing.JLabel();
        nomeprofessor = new javax.swing.JTextField();
        statusprofessor = new javax.swing.JTextField();
        bairroprofessor = new javax.swing.JTextField();
        cepprofessor = new javax.swing.JFormattedTextField();
        lblbairroprof = new javax.swing.JLabel();
        cidadeprofessor = new javax.swing.JTextField();
        celularprofessor = new javax.swing.JFormattedTextField();
        lblcidadeprof = new javax.swing.JLabel();
        emailprofessor = new javax.swing.JTextField();
        lblemailprof = new javax.swing.JLabel();
        lblrgprof = new javax.swing.JLabel();
        lblcpfprof = new javax.swing.JLabel();
        rgprofessor = new javax.swing.JTextField();
        telefoneprofessor = new javax.swing.JFormattedTextField();
        lbltelprof = new javax.swing.JLabel();
        cpfprofessor = new javax.swing.JFormattedTextField();
        lblcelprof = new javax.swing.JLabel();
        saidaprofessor = new javax.swing.JFormattedTextField();
        lblenderecoprof = new javax.swing.JLabel();
        lbldtaeprof = new javax.swing.JLabel();
        lblcodigoprof = new javax.swing.JLabel();
        lbldtasprof = new javax.swing.JLabel();
        lblnomeprof = new javax.swing.JLabel();
        lblnascprof = new javax.swing.JLabel();
        adicionarprof = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        lbldicprof.setText("Disciplina Lecionada:");

        lblcepprof.setText("Cep:");

        lblestadoprof.setText("Estado:");

        lblloginprof.setForeground(new java.awt.Color(255, 51, 51));
        lblloginprof.setText("(Login)");

        lblsenhaprof.setForeground(new java.awt.Color(255, 0, 0));
        lblsenhaprof.setText("(Senha)");

        codprofessor.setEditable(false);
        codprofessor.setEnabled(false);

        try {
            datanascimentoprofessor.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("## / ## / ####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        datanascimentoprofessor.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        estadoprofessor.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "MG", "SP", "AL", "DF" }));

        lblstatusprof.setText("Status:");

        lblbairroprof.setText("Bairro:");

        lblcidadeprof.setText("Cidade:");

        lblemailprof.setText("E-mail:");

        lblrgprof.setText("RG:");

        lblcpfprof.setText("CPF:");

        lbltelprof.setText("Telefone Residencial");

        lblcelprof.setText("Celular:");

        lblenderecoprof.setText("Endereco:");

        lbldtaeprof.setText("Data de entrada:");

        lblcodigoprof.setText("Codigo");

        lbldtasprof.setText("Data de saida:");

        lblnomeprof.setText("Nome do professor:");

        lblnascprof.setText("Nascimento:");

        adicionarprof.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/infox/icones/new-file 48.png"))); // NOI18N
        adicionarprof.setText("Adicionar");
        adicionarprof.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        adicionarprof.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        adicionarprof.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        adicionarprof.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                adicionarprofActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(rgprofessor)
                            .addComponent(lbldtaeprof, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(entradaprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cpfprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbldtasprof, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(saidaprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lbldicprof, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(telefoneprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(celularprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lblstatusprof, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(disciplinaprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(statusprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(codprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(20, 20, 20)
                        .addComponent(nomeprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, 430, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblenderecoprof, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(390, 390, 390)
                        .addComponent(lblnascprof, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(endprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, 440, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(20, 20, 20)
                        .addComponent(datanascimentoprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblbairroprof, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(410, 410, 410)
                        .addComponent(lblcepprof))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblrgprof, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(90, 90, 90)
                        .addComponent(lblcpfprof, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(lblsenhaprof)
                        .addGap(72, 72, 72)
                        .addComponent(lbltelprof, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(20, 20, 20)
                        .addComponent(lblcelprof, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(bairroprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, 440, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(cepprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblcodigoprof, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(80, 80, 80)
                        .addComponent(lblnomeprof, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblloginprof, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblcidadeprof, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(400, 400, 400)
                        .addComponent(lblestadoprof))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(cidadeprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, 440, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(20, 20, 20)
                        .addComponent(estadoprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(lblemailprof, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(emailprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, 560, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(adicionarprof)
                .addGap(247, 247, 247))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblcodigoprof)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblnomeprof)
                        .addComponent(lblloginprof)))
                .addGap(6, 6, 6)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(codprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nomeprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblenderecoprof)
                    .addComponent(lblnascprof))
                .addGap(6, 6, 6)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(endprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(datanascimentoprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblbairroprof)
                    .addComponent(lblcepprof))
                .addGap(6, 6, 6)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bairroprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cepprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblcidadeprof)
                    .addComponent(lblestadoprof))
                .addGap(6, 6, 6)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cidadeprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(estadoprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addComponent(lblemailprof)
                .addGap(6, 6, 6)
                .addComponent(emailprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblrgprof)
                    .addComponent(lblcpfprof)
                    .addComponent(lblsenhaprof)
                    .addComponent(lbltelprof)
                    .addComponent(lblcelprof))
                .addGap(6, 6, 6)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(rgprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cpfprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(20, 20, 20)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lbldtaeprof)
                            .addComponent(lbldtasprof)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(telefoneprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(celularprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(20, 20, 20)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(lbldicprof)
                                    .addComponent(lblstatusprof))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(disciplinaprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(statusprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(saidaprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(entradaprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(18, 18, 18)
                .addComponent(adicionarprof)
                .addContainerGap(83, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        setSize(new java.awt.Dimension(596, 581));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void adicionarprofActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_adicionarprofActionPerformed
        alterar();
        Tela_Professor.Atualizar.doClick();
    }//GEN-LAST:event_adicionarprofActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Tela_Editar_Professor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Tela_Editar_Professor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Tela_Editar_Professor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Tela_Editar_Professor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Tela_Editar_Professor().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton adicionarprof;
    private javax.swing.JTextField bairroprofessor;
    private javax.swing.JFormattedTextField celularprofessor;
    private javax.swing.JFormattedTextField cepprofessor;
    private javax.swing.JTextField cidadeprofessor;
    private javax.swing.JTextField codprofessor;
    private javax.swing.JFormattedTextField cpfprofessor;
    private javax.swing.JFormattedTextField datanascimentoprofessor;
    private javax.swing.JTextField disciplinaprofessor;
    private javax.swing.JTextField emailprofessor;
    private javax.swing.JTextField endprofessor;
    private javax.swing.JFormattedTextField entradaprofessor;
    private javax.swing.JComboBox<String> estadoprofessor;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lblbairroprof;
    private javax.swing.JLabel lblcelprof;
    private javax.swing.JLabel lblcepprof;
    private javax.swing.JLabel lblcidadeprof;
    private javax.swing.JLabel lblcodigoprof;
    private javax.swing.JLabel lblcpfprof;
    private javax.swing.JLabel lbldicprof;
    private javax.swing.JLabel lbldtaeprof;
    private javax.swing.JLabel lbldtasprof;
    private javax.swing.JLabel lblemailprof;
    private javax.swing.JLabel lblenderecoprof;
    private javax.swing.JLabel lblestadoprof;
    private javax.swing.JLabel lblloginprof;
    private javax.swing.JLabel lblnascprof;
    private javax.swing.JLabel lblnomeprof;
    private javax.swing.JLabel lblrgprof;
    private javax.swing.JLabel lblsenhaprof;
    private javax.swing.JLabel lblstatusprof;
    private javax.swing.JLabel lbltelprof;
    private javax.swing.JTextField nomeprofessor;
    private javax.swing.JTextField rgprofessor;
    private javax.swing.JFormattedTextField saidaprofessor;
    private javax.swing.JTextField statusprofessor;
    private javax.swing.JFormattedTextField telefoneprofessor;
    // End of variables declaration//GEN-END:variables
}
