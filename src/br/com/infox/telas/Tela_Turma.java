/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.infox.telas;

import br.com.infox.dal.ModoConexao;
import java.awt.Dimension;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.JOptionPane;
import net.proteanit.sql.DbUtils;

/**
 *
 * @author Wendrews DJE
 */
public class Tela_Turma extends javax.swing.JInternalFrame {

    Connection conexao = null;
    //criando variaveis especiais para conexao com o banco
    //PreparedStatement e ResultSet são frameworks do pacote java.sql
    //e serve para preparar e executar as isntruções SQL
    PreparedStatement pst = null;
    ResultSet rs = null;

    private String tipo;

    public Tela_Turma() {
        initComponents();

        conexao = ModoConexao.conector();

        pesquisar_curso();
        pesquisar_turma();

        idTurma.setVisible(false);
        Atualizar.setVisible(false);

    }

    private void pesquisar_curso() {
        String sql = "select idcurso as Id, nomecurso as Curso from tbcurso where nomecurso like ?";
//           String sql = "select idcurso , nomecurso  from tbcurso";
        try {
            pst = conexao.prepareStatement(sql);
            //passando o conteudo da caixa de pesquisa para o ?
            //atenção ao "%" - continuação da string sql
            pst.setString(1, txtNomeCurso.getText() + "%");
            rs = pst.executeQuery();
            //a linha abaixo usa a biblioteca rs2zml.jar para preencher a tabela
            tbCurso.setModel(DbUtils.resultSetToTableModel(rs));
        } catch (Exception e) {
        }

    }

    private void pesquisar_turma() {
        if (cmbTurma.getSelectedItem().equals("Nome da Turma")) {
            String sql = "select idturma as Codigo, nometurma as Nome_da_Turma,"
                    + "sala as Sala, nomecurso as Curso, periodo as Turno,"
                    + "quant as Quantidade_de_Alunos,"
                    + "situacao as Situacao from tbturma where nometurma like ? order by nometurma";
//           String sql = "select idcurso , nomecurso  from tbcurso";
            try {
                pst = conexao.prepareStatement(sql);
                //passando o conteudo da caixa de pesquisa para o ?
                //atenção ao "%" - continuação da string sql
                pst.setString(1, txtPesquisaTurma.getText() + "%");
                rs = pst.executeQuery();
                //a linha abaixo usa a biblioteca rs2zml.jar para preencher a tabela
                tblTurma.setModel(DbUtils.resultSetToTableModel(rs));
            } catch (Exception e) {
            }

        } else if (cmbTurma.getSelectedItem().equals("Curso")) {
            String sql = "select idturma as Codigo, nometurma as Nome_da_Turma,"
                    + "sala as Sala, nomecurso as Curso, periodo as Turno,"
                    + "quant as Quantidade_de_Alunos,"
                    + "situacao as Situacao from tbturma where nomecurso like ? order by nomecurso";
//           String sql = "select idcurso , nomecurso  from tbcurso";
            try {
                pst = conexao.prepareStatement(sql);
                //passando o conteudo da caixa de pesquisa para o ?
                //atenção ao "%" - continuação da string sql
                pst.setString(1, txtPesquisaTurma.getText() + "%");
                rs = pst.executeQuery();
                //a linha abaixo usa a biblioteca rs2zml.jar para preencher a tabela
                tblTurma.setModel(DbUtils.resultSetToTableModel(rs));
            } catch (Exception e) {
            }

        } else if (cmbTurma.getSelectedItem().equals("Situacao")) {
            String sql = "select idturma as Codigo, nometurma as Nome_da_Turma,"
                    + "sala as Sala, nomecurso as Curso, periodo as Turno,"
                    + "quant as Quantidade_de_Alunos,"
                    + "situacao as Situacao from tbturma where situacao like ? order by situacao";
//           String sql = "select idcurso , nomecurso  from tbcurso";
            try {
                pst = conexao.prepareStatement(sql);
                //passando o conteudo da caixa de pesquisa para o ?
                //atenção ao "%" - continuação da string sql
                pst.setString(1, txtPesquisaTurma.getText() + "%");
                rs = pst.executeQuery();
                //a linha abaixo usa a biblioteca rs2zml.jar para preencher a tabela
                tblTurma.setModel(DbUtils.resultSetToTableModel(rs));
            } catch (Exception e) {
            }

        }

    }

//    private void pesquisar_curso_teste() {
//        String sql = "select nomecurso from tbcurso";
//        try {
//            pst = conexao.prepareStatement(sql);
//            //passando o conteudo da caixa de pesquisa para o ?
//            //atenção ao "%" - continuação da string sql
//
//            rs = pst.executeQuery();
//            //a linha abaixo usa a biblioteca rs2zml.jar para preencher a tabela
//            cboCurso.setSelectedItem(rs);;
//        } catch (Exception e) {
//        }
//    }
    private void adicionar_turma() {
        String sql = "insert into tbturma (idcurso,nomecurso,nometurma,situacao,sala,datacad,quant,periodo) VALUES (?,?,?,?,?,?,?,?)";
        try {
            pst = conexao.prepareStatement(sql);

            pst.setString(1, txtCodCurso.getText());
            pst.setString(2, txtCursoNome.getText());
            pst.setString(3, txtNomeTurma.getText());
            pst.setString(4, txtSituacao.getSelectedItem().toString());
            pst.setString(5, txtSala.getText());
            pst.setString(6, txtCad.getText());
            pst.setString(7, txtQuant.getText());
            pst.setString(8, cboPeriodo.getSelectedItem().toString());

            if ((txtSituacao.getSelectedItem().toString().isEmpty()) || (txtSala.getText().isEmpty())
                    || (txtNomeTurma.getText().isEmpty())
                    || (txtCad.getText().isEmpty()) || (txtQuant.getText().isEmpty())
                    || (txtCodCurso.getText().isEmpty()) || (txtCursoNome.getText().isEmpty())) {
                JOptionPane.showMessageDialog(null, "Preencha todos os campos obrigatorios!");

            } else {

                //a linha abaixo atualiza a tabela usuario com os dados do formulario
                //a estrutura abaixo é usada para confirmar a inserção dos dados na tabela
                int adicionado = pst.executeUpdate();
                if (adicionado > 0) {
                    JOptionPane.showMessageDialog(null, "Dados adicionados com Sucesso");
                    // codprofessor.setText(null);
                    txtCodTurma.setText(null);
//                    txtSituacao.setSelectedItem(null);
                    txtSala.setText(null);
                    txtNomeTurma.setText(null);
                    txtCad.setText(null);
                    txtQuant.setText(null);
                    txtCodCurso.setText(null);
                    txtCursoNome.setText(null);

                }
            }

        } catch (Exception e) {
            if (e == e) {
                JOptionPane.showMessageDialog(null, "Selecione um curso");

            } else {
                JOptionPane.showMessageDialog(null, e);
                System.out.println(e);
            }
        }
    }

    private void alterar() {
        String sql = "update tbturma set nomecurso=?,nometurma=?,situacao=?,sala=?,datacad=?,"
                + "quant=?,periodo=? where idturma=?";
        try {
            pst = conexao.prepareStatement(sql);

            pst.setString(1, txtCursoNome.getText());
            pst.setString(2, txtNomeTurma.getText());
            pst.setString(3, txtSituacao.getSelectedItem().toString());
            pst.setString(4, txtSala.getText());
            pst.setString(5, txtCad.getText());
            pst.setString(6, txtQuant.getText());
            pst.setString(7, cboPeriodo.getSelectedItem().toString());
            pst.setString(8, txtCodCurso.getText());
            //validação dos campos obrigatorios
            if ((txtSituacao.getSelectedItem().toString().isEmpty()) || (txtSala.getText().isEmpty())
                    || (txtNomeTurma.getText().isEmpty())
                    || (txtCad.getText().isEmpty()) || (txtQuant.getText().isEmpty())
                    || (txtCodCurso.getText().isEmpty()) || (txtCursoNome.getText().isEmpty())) {
                JOptionPane.showMessageDialog(null, "Preencha todos os campos obrigatorios!");

            } else {

                //a linha abaixo atualiza a tabela usuario com os dados do formulario
                //a estrutura abaixo é usada para confirmar a inserção dos dados na tabela
                int adicionado = pst.executeUpdate();
                if (adicionado > 0) {
                    JOptionPane.showMessageDialog(null, "Turma alterado com sucesso");

                    txtCodTurma.setText(null);
//                   txtSituacao.setSelectedItem(null);
                    txtSala.setText(null);
                    txtNomeTurma.setText(null);
                    txtCad.setText(null);
                    txtQuant.setText(null);
                    txtCodCurso.setText(null);
                    txtCursoNome.setText(null);

                }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    private void remover() {
        int confirma = JOptionPane.showConfirmDialog(null, "Tem certeza de que deseja remover esta turma?", "Atenção", JOptionPane.YES_NO_OPTION);
        if (confirma == JOptionPane.YES_OPTION) {
            String sql = "delete from tbturma where idusu=?;";

            try {
                pst = conexao.prepareStatement(sql);
                pst.setString(1, txtCodTurma.getText());

                int apagado = pst.executeUpdate();
                if (apagado > 0) {
                    JOptionPane.showMessageDialog(null, "Usuario excluido com sucesso");

                    txtCodTurma.setText(null);
//                    txtSituacao.setSelectedItem(null);
                    txtSala.setText(null);
                    txtNomeTurma.setText(null);
                    txtCad.setText(null);
                    txtQuant.setText(null);
                    txtCodCurso.setText(null);
                    txtCursoNome.setText(null);

                }

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }
        }
    }

    public void setar_campos() {

        int setar = tbCurso.getSelectedRow();
        txtCodCurso.setText(tbCurso.getModel().getValueAt(setar, 0).toString());
        txtCursoNome.setText(tbCurso.getModel().getValueAt(setar, 1).toString());
    }

    public void setar() {

        int setar = tblTurma.getSelectedRow();
        idTurma.setText(tblTurma.getModel().getValueAt(setar, 0).toString());

    }

//    
    public void setPosicao() {
        Dimension d = this.getDesktopPane().getSize();
        this.setLocation((d.width - this.getSize().width) / 2, (d.height - this.getSize().height) / 2);

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblTurma = new javax.swing.JTable();
        jLabel11 = new javax.swing.JLabel();
        txtPesquisaTurma = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        cmbTurma = new javax.swing.JComboBox<>();
        idTurma = new javax.swing.JTextField();
        Atualizar = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        txtNomeCurso = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbCurso = new javax.swing.JTable();
        txtCodCurso = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        txtCursoNome = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        txtCodTurma = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtSala = new javax.swing.JTextField();
        txtNomeTurma = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtCad = new javax.swing.JFormattedTextField();
        jLabel5 = new javax.swing.JLabel();
        txtQuant = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        cboPeriodo = new javax.swing.JComboBox<>();
        btnAdicionar = new javax.swing.JButton();
        jLabel13 = new javax.swing.JLabel();
        txtSituacao = new javax.swing.JComboBox<>();

        setClosable(true);
        setIconifiable(true);

        tblTurma.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Codigo", "Nome", "Data Criacao"
            }
        ));
        tblTurma.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblTurmaMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tblTurma);

        jLabel11.setText("Localizar turma ");

        txtPesquisaTurma.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPesquisaTurmaKeyReleased(evt);
            }
        });

        jLabel12.setText("Pesquisar por:");

        cmbTurma.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Nome da Turma", "Situacao", "Curso" }));
        cmbTurma.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbTurmaActionPerformed(evt);
            }
        });

        Atualizar.setText("Atualizar");
        Atualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AtualizarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtPesquisaTurma, javax.swing.GroupLayout.PREFERRED_SIZE, 284, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmbTurma, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(258, 258, 258)
                        .addComponent(idTurma, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Atualizar)
                        .addGap(0, 23, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(txtPesquisaTurma, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12)
                    .addComponent(cmbTurma, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(idTurma, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Atualizar))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 295, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Pesquisar Turma", jPanel5);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Selecioinar Curso"));

        txtNomeCurso.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtNomeCursoKeyReleased(evt);
            }
        });

        jLabel10.setText("Pesquisar:");

        tbCurso.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tbCurso.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbCursoMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tbCurso);

        txtCodCurso.setEditable(false);
        txtCodCurso.setText(" ");
        txtCodCurso.setEnabled(false);

        jLabel8.setText("Código Curso");

        jLabel9.setText("Nome Curso");

        txtCursoNome.setEditable(false);
        txtCursoNome.setText(" ");
        txtCursoNome.setEnabled(false);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 504, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtNomeCurso, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8)
                            .addComponent(txtCodCurso, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel9)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(txtCursoNome))))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNomeCurso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(jLabel9))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCodCurso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCursoNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(19, 19, 19))
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Cadastro de Turma"));

        txtCodTurma.setEditable(false);
        txtCodTurma.setEnabled(false);

        jLabel1.setText("Codigo");

        jLabel3.setText("Sala");

        jLabel4.setText("Nome da Turma / Curso");

        try {
            txtCad.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("## / ## / ####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtCad.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jLabel5.setText("Data de Cadastro");

        jLabel6.setText("Quantidade/Aluno");

        jLabel7.setText("Periodo");

        cboPeriodo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Manha", "Tarde", "Noite", "Integral" }));

        btnAdicionar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/infox/icones/new-file 48.png"))); // NOI18N
        btnAdicionar.setText("Adicionar");
        btnAdicionar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnAdicionar.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btnAdicionar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnAdicionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAdicionarActionPerformed(evt);
            }
        });

        jLabel13.setText("Situacao");

        txtSituacao.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Ativo", "Inativo" }));

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(btnAdicionar, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtQuant, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel13)
                                    .addComponent(txtSituacao, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7)
                            .addComponent(cboPeriodo, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jLabel4)
                        .addComponent(txtNomeTurma)
                        .addGroup(jPanel4Layout.createSequentialGroup()
                            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel1)
                                .addComponent(txtCodTurma, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGap(18, 18, 18)
                            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(txtCad, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel5))
                            .addGap(18, 18, 18)
                            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel3)
                                .addComponent(txtSala, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtCodTurma, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtSala, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtCad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtNomeTurma, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(jLabel13)
                            .addComponent(jLabel7)))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(38, 38, 38)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtSituacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtQuant, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cboPeriodo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 35, Short.MAX_VALUE)
                .addComponent(btnAdicionar)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(52, 52, 52)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(28, 28, 28))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Cadastrar Turmas", jPanel1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tblTurmaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblTurmaMouseClicked
        setar();
        int confirma = JOptionPane.showConfirmDialog(null, "deseja alterar os dados desta turma?", "Atenção", JOptionPane.YES_NO_OPTION);
        if (confirma == JOptionPane.YES_OPTION) {

            Editar_Turma edt = new Editar_Turma();
            edt.setVisible(true);

        } else {

        }
    }//GEN-LAST:event_tblTurmaMouseClicked

    private void txtPesquisaTurmaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPesquisaTurmaKeyReleased
        pesquisar_turma();
    }//GEN-LAST:event_txtPesquisaTurmaKeyReleased

    private void txtNomeCursoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNomeCursoKeyReleased
        pesquisar_curso();
    }//GEN-LAST:event_txtNomeCursoKeyReleased

    private void tbCursoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbCursoMouseClicked
        setar_campos();
    }//GEN-LAST:event_tbCursoMouseClicked

    private void btnAdicionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAdicionarActionPerformed
        adicionar_turma();
        Atualizar.doClick();
    }//GEN-LAST:event_btnAdicionarActionPerformed

    private void AtualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AtualizarActionPerformed
        pesquisar_turma();
    }//GEN-LAST:event_AtualizarActionPerformed

    private void cmbTurmaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbTurmaActionPerformed
        pesquisar_turma();
    }//GEN-LAST:event_cmbTurmaActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JButton Atualizar;
    private javax.swing.JButton btnAdicionar;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox<String> cboPeriodo;
    private javax.swing.JComboBox<String> cmbTurma;
    public static javax.swing.JTextField idTurma;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable tbCurso;
    private javax.swing.JTable tblTurma;
    private javax.swing.JFormattedTextField txtCad;
    private javax.swing.JTextField txtCodCurso;
    private javax.swing.JTextField txtCodTurma;
    private javax.swing.JTextField txtCursoNome;
    private javax.swing.JTextField txtNomeCurso;
    private javax.swing.JTextField txtNomeTurma;
    private javax.swing.JTextField txtPesquisaTurma;
    private javax.swing.JTextField txtQuant;
    private javax.swing.JTextField txtSala;
    private javax.swing.JComboBox<String> txtSituacao;
    // End of variables declaration//GEN-END:variables
}
