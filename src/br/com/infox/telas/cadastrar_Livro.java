/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.infox.telas;

import br.com.infox.dal.ModoConexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.JOptionPane;

public class cadastrar_Livro extends javax.swing.JFrame {

    Connection conexao = null;
    // criando variaveis especias para a conexao com o banco
    // PreparedStantement e Resulted são frameworks do  pacote java.sql e serve para preparar e executar as instruçoes SQL
    PreparedStatement pst = null;
    ResultSet rs = null;

    public cadastrar_Livro() {

        initComponents();
        conexao = ModoConexao.conector();
        situacao.setVisible(false);
        idLivro.setVisible(false);
        
        idLivro.setText(Tela_Biblioteca.idLivro.getText());
        consultar();

    }

    private void adicionar() {
        String sql = "insert into tbbiblioteca(codbarras,nomelivro,"
                + "categoria,localizacao,edicao,autor,editora,situacao) value(?,?,?,?,?,?,?,?)";
        try {
            pst = conexao.prepareStatement(sql);
            //pst.setString(1, codigolivro.getText());
            pst.setString(1, codBarras.getText());
            pst.setString(2, nomeLivro.getText());
            pst.setString(3, categoria.getSelectedItem().toString());
            pst.setString(4, localizacao.getText());
            pst.setString(5, edicao.getText());
            pst.setString(6, autor.getText());
            pst.setString(7, editora.getText());
            pst.setString(8, situacao.getText());

            if ((codBarras.getText().isEmpty())
                    || (nomeLivro.getText().isEmpty())
                    || (localizacao.getText().isEmpty())
                    || (edicao.getText().isEmpty())
                    || (autor.getText().isEmpty())
                    || (editora.getText().isEmpty())) {

                JOptionPane.showMessageDialog(null, "Preencha todos os campos!");

            } else {
                int adicionado = pst.executeUpdate();
                if (adicionado > 0) {
                    JOptionPane.showMessageDialog(null, "Livro cadastrado");
                    cod.setText(null);
                    codBarras.setText(null);
                    localizacao.setText(null);
                    nomeLivro.setText(null);
                    edicao.setText(null);
                    autor.setText(null);
                    editora.setText(null);

                }
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    private void alterar() {
        String sql = "update tbbiblioteca set codbarras=?,nomelivro=?,"
                + "categoria=?,localizacao=?,edicao=?,autor=?,editora=? where idlivro=?";

//        idaluno=?,nomealuno=?,"
//                + "dataentrega=?,datadedevolucao=?,situacao=?
        try {
            pst = conexao.prepareStatement(sql);

            
            pst.setString(1, codBarras.getText());
            pst.setString(2, nomeLivro.getText());
            pst.setString(3, categoria.getSelectedItem().toString());
            pst.setString(4, localizacao.getText());
            pst.setString(5, edicao.getText());
            pst.setString(6, autor.getText());
            pst.setString(7, editora.getText());
//            pst.setString(8, situacao.getText());
            pst.setString(8, cod.getText());
//            pst.setString(9,idAluno.getText());
//            pst.setString(10,nomeAluno.getText());
//            pst.setString(11, dataEntrega.getText());
//            pst.setString(12, dataDevolucao.getText());
//            pst.setString(13,situacao.getText());
//            pst.setString(14, idBiblioteca.getText());

            if ((cod.getText().isEmpty())
                    || (codBarras.getText().isEmpty())
                    || (nomeLivro.getText().isEmpty())
                    || (localizacao.getText().isEmpty())
                    || (edicao.getText().isEmpty())
                    || (autor.getText().isEmpty())
                    || (editora.getText().isEmpty())) {

                JOptionPane.showMessageDialog(null, "Preencha todos os campos!");

            } else {
                int adicionado = pst.executeUpdate();
                if (adicionado > 0) {
                    JOptionPane.showMessageDialog(null, "Alteracoes realizadas com sucesso");
                    cod.setText(null);
                    codBarras.setText(null);
                    nomeLivro.setText(null);
                    localizacao.setText(null);
                    edicao.setText(null);
                    autor.setText(null);
                    editora.setText(null);
//                    idAluno.setText(null);
//                    nomeAluno.setText(null);
//                    dataEntrega.setText(null);
//                    dataDevolucao.setText(null);
//                    situacao.setText(null);
//                    idBiblioteca.setText(null);

                }
            }

        } catch (Exception e) {

            JOptionPane.showMessageDialog(null, e);
        }

    }

    private void consultar() {
        cod.setText(idLivro.getText());
        String sql = "select * from tbbiblioteca where idlivro=?";
        try {
            pst = conexao.prepareStatement(sql);
            pst.setString(1, cod.getText());
            rs = pst.executeQuery();
            if (rs.next()) {

                cod.setText(rs.getString(1));
                codBarras.setText(rs.getString(2));
                localizacao.setText(rs.getString(5));
                categoria.setSelectedItem(rs.getString(4));
                nomeLivro.setText(rs.getString(3));
                edicao.setText(rs.getString(6));
                autor.setText(rs.getString(7));
                editora.setText(rs.getString(8));

            } else {
//                JOptionPane.showMessageDialog(null, "Usuario não cadastrado");
                cod.setText(null);
                codBarras.setText(null);
                localizacao.setText(null);
                nomeLivro.setText(null);
                edicao.setText(null);
                autor.setText(null);
                editora.setText(null);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jCheckBox1 = new javax.swing.JCheckBox();
        jCheckBox2 = new javax.swing.JCheckBox();
        jCheckBox3 = new javax.swing.JCheckBox();
        jCheckBox4 = new javax.swing.JCheckBox();
        jCheckBox5 = new javax.swing.JCheckBox();
        jCheckBox6 = new javax.swing.JCheckBox();
        cod = new javax.swing.JTextField();
        codBarras = new javax.swing.JTextField();
        nomeLivro = new javax.swing.JTextField();
        localizacao = new javax.swing.JTextField();
        categoria = new javax.swing.JComboBox<>();
        edicao = new javax.swing.JTextField();
        autor = new javax.swing.JTextField();
        editora = new javax.swing.JTextField();
        situacao = new javax.swing.JTextField();
        btnadicionarlivro = new javax.swing.JButton();
        btnalterarlivro = new javax.swing.JButton();
        idLivro = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setText("Codigo");

        jLabel2.setText("Codigo de Barras ");

        jLabel3.setText("Titulo do Livro");

        jLabel4.setText("Edicao / Volume");

        jLabel5.setText("Categoria");

        jLabel6.setText("Localizacao do Livro");

        jLabel7.setText("Autor ( a )");

        jLabel8.setText("Editora");

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Tipo"));

        jCheckBox1.setText("Brochura");

        jCheckBox2.setText("Capa Dura");

        jCheckBox3.setText("Revista");

        jCheckBox4.setText("Outros");

        jCheckBox5.setText("Tecnico");

        jCheckBox6.setText("CD / DVD");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jCheckBox1)
                .addGap(58, 58, 58)
                .addComponent(jCheckBox2)
                .addGap(56, 56, 56)
                .addComponent(jCheckBox3)
                .addGap(81, 81, 81)
                .addComponent(jCheckBox5)
                .addGap(73, 73, 73)
                .addComponent(jCheckBox6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jCheckBox4)
                .addGap(15, 15, 15))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCheckBox1)
                    .addComponent(jCheckBox2)
                    .addComponent(jCheckBox3)
                    .addComponent(jCheckBox5)
                    .addComponent(jCheckBox6)
                    .addComponent(jCheckBox4))
                .addContainerGap(15, Short.MAX_VALUE))
        );

        cod.setEditable(false);
        cod.setEnabled(false);

        categoria.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Acao", "Romance", "Aventura", "Ficcao", "Cientifico", "Terror", "Academico", "Outros" }));

        situacao.setText("Disponivel");
        situacao.setFocusCycleRoot(true);

        btnadicionarlivro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/infox/icones/new-file 48.png"))); // NOI18N
        btnadicionarlivro.setText("Adicionar");
        btnadicionarlivro.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnadicionarlivro.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btnadicionarlivro.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnadicionarlivro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnadicionarlivroActionPerformed(evt);
            }
        });

        btnalterarlivro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/infox/icones/new-file 48.png"))); // NOI18N
        btnalterarlivro.setText("Alterar");
        btnalterarlivro.setEnabled(false);
        btnalterarlivro.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnalterarlivro.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btnalterarlivro.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnalterarlivro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnalterarlivroActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(231, 231, 231)
                        .addComponent(idLivro, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(51, 51, 51)
                        .addComponent(situacao, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLabel1)
                        .addGap(124, 124, 124)
                        .addComponent(jLabel2)
                        .addGap(135, 135, 135)
                        .addComponent(jLabel3))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(cod, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(codBarras, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(nomeLivro, javax.swing.GroupLayout.PREFERRED_SIZE, 352, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLabel5)
                        .addGap(110, 110, 110)
                        .addComponent(jLabel6)
                        .addGap(125, 125, 125)
                        .addComponent(jLabel4))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(categoria, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(localizacao, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(edicao, javax.swing.GroupLayout.PREFERRED_SIZE, 352, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLabel7)
                        .addGap(327, 327, 327)
                        .addComponent(jLabel8))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(autor, javax.swing.GroupLayout.PREFERRED_SIZE, 359, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(editora, javax.swing.GroupLayout.PREFERRED_SIZE, 352, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(272, 272, 272)
                        .addComponent(btnadicionarlivro, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(71, 71, 71)
                        .addComponent(btnalterarlivro, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 38, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(situacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(idLivro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3))
                .addGap(2, 2, 2)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cod, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(codBarras, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nomeLivro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6)
                    .addComponent(jLabel4))
                .addGap(6, 6, 6)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(categoria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(localizacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(edicao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8))
                .addGap(6, 6, 6)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(autor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(editora, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnadicionarlivro)
                    .addComponent(btnalterarlivro)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        setSize(new java.awt.Dimension(803, 494));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnadicionarlivroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnadicionarlivroActionPerformed
        adicionar();
        Tela_Biblioteca.Atualizar.doClick();
    }//GEN-LAST:event_btnadicionarlivroActionPerformed

    private void btnalterarlivroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnalterarlivroActionPerformed
        alterar();
        Tela_Biblioteca.Atualizar.doClick();
        this.dispose();
    }//GEN-LAST:event_btnalterarlivroActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(cadastrar_Livro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(cadastrar_Livro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(cadastrar_Livro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(cadastrar_Livro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new cadastrar_Livro().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField autor;
    public static javax.swing.JButton btnadicionarlivro;
    public static javax.swing.JButton btnalterarlivro;
    private javax.swing.JComboBox<String> categoria;
    private javax.swing.JTextField cod;
    private javax.swing.JTextField codBarras;
    private javax.swing.JTextField edicao;
    private javax.swing.JTextField editora;
    private javax.swing.JTextField idLivro;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JCheckBox jCheckBox2;
    private javax.swing.JCheckBox jCheckBox3;
    private javax.swing.JCheckBox jCheckBox4;
    private javax.swing.JCheckBox jCheckBox5;
    private javax.swing.JCheckBox jCheckBox6;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JTextField localizacao;
    private javax.swing.JTextField nomeLivro;
    private javax.swing.JTextField situacao;
    // End of variables declaration//GEN-END:variables
}
