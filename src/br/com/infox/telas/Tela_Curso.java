package br.com.infox.telas;

import java.sql.*;
import br.com.infox.dal.ModoConexao;
import static br.com.infox.telas.Tela_Disciplina.idDisciplina;
import java.awt.Dimension;
import javax.swing.JOptionPane;
import net.proteanit.sql.DbUtils;

public class Tela_Curso extends javax.swing.JInternalFrame {

    //Usando a variavel conexao do DAL
    Connection conexao = null;
    //criando variaveis especiais para conexao com o banco
    //PreparedStatement e ResultSet são frameworks do pacote java.sql
    //e serve para preparar e executar as isntruções SQL
    PreparedStatement pst = null;
    ResultSet rs = null;

    /**
     * Creates new form Curso_turma
     */
    public Tela_Curso() {
        initComponents();
        conexao = ModoConexao.conector();

        pesquisar_curso();

    }

    private void adicionar_curso() {
        String sql = "insert into tbcurso ( nomecurso, valorcurso, totalhoras, quantmodulos, datacadastro, datatermino, duracaocurso)values(?,?,?,?,?,?,?)";
        try {
            pst = conexao.prepareStatement(sql);
            //  pst.setString(1, codigocurso.getText());
            pst.setString(1, nomecurso.getText());
            pst.setString(2, valorcurso.getText());
            pst.setString(3, totalhoras.getText());
            pst.setString(4, quantidademodulos.getSelectedItem().toString());
            pst.setString(5, datacadastrocurso.getText());
            pst.setString(6, dataterminocurso.getText());
            pst.setString(7, duracaocurso.getSelectedItem().toString());
            //validação dos campos obrigatorios
            if ((nomecurso.getText().isEmpty()) || (valorcurso.getText().isEmpty()) || (totalhoras.getText().isEmpty()) || (datacadastrocurso.getText().isEmpty()) || (dataterminocurso.getText().isEmpty())) {
                JOptionPane.showMessageDialog(null, "Preencha todos os campos obrigatorios!");
            } else {

                //a linha abaixo atualiza a tabela usuario com os dados do formulario
                //a estrutura abaixo é usada para confirmar a inserção dos dados na tabela
                int adicionado = pst.executeUpdate();
                if (adicionado > 0) {
                    JOptionPane.showMessageDialog(null, "Curso adicionado com sucesso");
                    codigocurso.setText(null);
                    nomecurso.setText(null);
                    valorcurso.setText(null);
                    totalhoras.setText(null);
                    quantidademodulos.setSelectedItem(null);
                    datacadastrocurso.setText(null);
                    dataterminocurso.setText(null);
                    duracaocurso.setSelectedItem(null);

                    btnAdicionarCurso.setEnabled(true);
                }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

//     private void editar_turma_tabela() {
//        int confirma = JOptionPane.showConfirmDialog(null, "Deseja editar esta turma?", "Atenção", JOptionPane.YES_NO_OPTION);
//        if (confirma == JOptionPane.YES_OPTION) {
//            Adicionar_Turma turma = new Adicionar_Turma();
//            turma.setVisible(true);
//
//        }
//    }
    private void pesquisar_curso() {
        if (cmbCurso.getSelectedItem().equals("Nome")) {
            String sql = "select idcurso as Id, nomecurso as Nome, valorcurso as Valor,"
                    + "totalhoras as Horas, quantmodulos as Modulos, datacadastro as Cadastro,"
                    + "datatermino as Termino,"
                    + " duracaocurso as Duracao from tbcurso where nomecurso like ? ORDER BY nomecurso ";
            try {
                pst = conexao.prepareStatement(sql);
                //passando o conteudo da caixa de pesquisa para o ?
                //atenção ao "%" - continuação da string sql
                pst.setString(1, pesqCurso.getText() + "%");
                rs = pst.executeQuery();
                //a linha abaixo usa a biblioteca rs2zml.jar para preencher a tabela
                tblCurso.setModel(DbUtils.resultSetToTableModel(rs));
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }

        } else if (cmbCurso.getSelectedItem().equals("Valor")) {
            String sql = "select idcurso as Id, nomecurso as Nome, valorcurso as Valor,"
                    + "totalhoras as Horas, quantmodulos as Modulos, datacadastro as Cadastro,"
                    + "datatermino as Termino,"
                    + " duracaocurso as Duracao from tbcurso where valorcurso like ? ORDER BY valorcurso ";
            try {
                pst = conexao.prepareStatement(sql);
                //passando o conteudo da caixa de pesquisa para o ?
                //atenção ao "%" - continuação da string sql
                pst.setString(1, pesqCurso.getText() + "%");
                rs = pst.executeQuery();
                //a linha abaixo usa a biblioteca rs2zml.jar para preencher a tabela
                tblCurso.setModel(DbUtils.resultSetToTableModel(rs));
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }

        } else if (cmbCurso.getSelectedItem().equals("Modulos")) {
            String sql = "select idcurso as Id, nomecurso as Nome, valorcurso as Valor,"
                    + "totalhoras as Horas, quantmodulos as Modulos, datacadastro as Cadastro,"
                    + "datatermino as Termino,"
                    + " duracaocurso as Duracao from tbcurso where quantmodulos like ? ORDER BY quantmodulos ";
            try {
                pst = conexao.prepareStatement(sql);
                //passando o conteudo da caixa de pesquisa para o ?
                //atenção ao "%" - continuação da string sql
                pst.setString(1, pesqCurso.getText() + "%");
                rs = pst.executeQuery();
                //a linha abaixo usa a biblioteca rs2zml.jar para preencher a tabela
                tblCurso.setModel(DbUtils.resultSetToTableModel(rs));
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }

        }

    }

    private void remover_curso() {
        //a estrutura abaixo confirma a remoção do usuario
        int confirma = JOptionPane.showConfirmDialog(null, "Tem certeza que deseja remover este curso?", "Atenção", JOptionPane.YES_NO_OPTION);
        if (confirma == JOptionPane.YES_OPTION) {
            String sql = "delete from tbcurso where idcurso=?";
            try {
                pst = conexao.prepareStatement(sql);
                pst.setString(1, codigocurso.getText());
                int apagado = pst.executeUpdate();
                if (apagado > 0) {
                    JOptionPane.showMessageDialog(null, "Curso removido com sucesso");
                    codigocurso.setText(null);
                    nomecurso.setText(null);
                    valorcurso.setText(null);
                    totalhoras.setText(null);
                    quantidademodulos.setSelectedItem(null);
                    datacadastrocurso.setText(null);
                    dataterminocurso.setText(null);
                    duracaocurso.setSelectedItem(null);

                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }
        }
    }

    private void editar_curso() {
        String sql = "update tbcurso set nomecurso=?, valorcurso=?, "
                + "totalhoras=?,quantmodulos=?,datacadastro=?,"
                + "datatermino=?,duracaocurso=? where idcurso=?";
        try {

            pst = conexao.prepareStatement(sql);

            pst.setString(1, nomecurso.getText());
            pst.setString(2, valorcurso.getText());
            pst.setString(3, totalhoras.getText());
            pst.setString(4, quantidademodulos.getSelectedItem().toString());
            pst.setString(5, datacadastrocurso.getText());
            pst.setString(6, dataterminocurso.getText());
            pst.setString(7, duracaocurso.getSelectedItem().toString());
            //validação dos campos obrigatorios
            pst.setString(8, codigocurso.getText());

            if ((nomecurso.getText().isEmpty()) || (valorcurso.getText().isEmpty()) || (totalhoras.getText().isEmpty()) || (datacadastrocurso.getText().isEmpty()) || (dataterminocurso.getText().isEmpty())) {
                JOptionPane.showMessageDialog(null, "Preencha todos os campos obrigatorios!");
            } else {

                //a linha abaixo atualiza a tabela usuario com os dados do formulario
                //a estrutura abaixo é usada para confirmar a inserção dos dados na tabela
                int adicionado = pst.executeUpdate();
                if (adicionado > 0) {
                    JOptionPane.showMessageDialog(null, "alterações salvas com sucesso");
                    codigocurso.setText(null);
                    nomecurso.setText(null);
                    valorcurso.setText(null);
                    totalhoras.setText(null);
                    quantidademodulos.setSelectedItem(null);
                    datacadastrocurso.setText(null);
                    dataterminocurso.setText(null);
                    duracaocurso.setSelectedItem(null);

                    btnEditar.setEnabled(false);
                    btnAdicionarCurso.setEnabled(true);
                }

            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    public void setar_campos() {

        int setar = tblCurso.getSelectedRow();
        codigocurso.setText(tblCurso.getModel().getValueAt(setar, 0).toString());
        nomecurso.setText(tblCurso.getModel().getValueAt(setar, 1).toString());
        valorcurso.setText(tblCurso.getModel().getValueAt(setar, 2).toString());
        totalhoras.setText(tblCurso.getModel().getValueAt(setar, 3).toString());
        quantidademodulos.setSelectedItem(tblCurso.getModel().getValueAt(setar, 4).toString());
        datacadastrocurso.setText(tblCurso.getModel().getValueAt(setar, 5).toString());
        dataterminocurso.setText(tblCurso.getModel().getValueAt(setar, 6).toString());
        duracaocurso.setSelectedItem(tblCurso.getModel().getValueAt(setar, 7).toString());

        //a linha abaixo desabilitar o botao adicionar
        // btnaddusu.setEnabled(false);
    }

    public void setPosicao() {
        Dimension d = this.getDesktopPane().getSize();
        this.setLocation((d.width - this.getSize().width) / 2, (d.height - this.getSize().height) / 2);

    }

    private void Validar_Curso() {

        String sql = "select * from tbcurso where nomecurso=?  ";
        try {
            pst = conexao.prepareStatement(sql);
            pst.setString(1, nomecurso.getText());
            rs = pst.executeQuery();

            if (rs.next()) {

                int confirma = JOptionPane.showConfirmDialog(null, "Deseja editar este curso?", "Atenção", JOptionPane.YES_NO_OPTION);
                if (confirma == JOptionPane.YES_OPTION) {
                    btnEditar.setEnabled(true);
                    btnAdicionarCurso.setEnabled(false);
                } else {
                    btnAdicionarCurso.setEnabled(true);
                    btnEditar.setEnabled(false);
                    codigocurso.setText(null);
                    nomecurso.setText(null);
                    valorcurso.setText(null);
                    totalhoras.setText(null);
                    quantidademodulos.setSelectedItem(null);
                    datacadastrocurso.setText(null);
                    dataterminocurso.setText(null);
                    duracaocurso.setSelectedItem(null);
                }

            } else {

            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

//    private void consultar() {
//        String sql = "select * from tbcurso where nomecurso=?";
//        try {
//            pst = conexao.prepareStatement(sql);
//            pst.setString(1, nomecurso.getText());
//            rs = pst.executeQuery();
//            if (rs.next()) {
//                
//                valorcurso.setText(rs.getString(3));
//                totalhoras.setText(rs.getString(4));
//                quantidademodulos.setSelectedItem(rs.getString(5));
//                datacadastrocurso.setText(rs.getString(6));
//                dataterminocurso.setText(rs.getString(7));
//                duracaocurso.setSelectedItem(rs.getString(8));
//            } else {
//                JOptionPane.showMessageDialog(null, "Usuario não cadastrado");
//                // as linhas abaixo limpam os campos
//                    nomecurso.setText(null);
//                    valorcurso.setText(null);
//                    totalhoras.setText(null);
//                    quantidademodulos.setSelectedItem(null);
//                    datacadastrocurso.setText(null);
//                    dataterminocurso.setText(null);
//                    duracaocurso.setSelectedItem(null);
//            }
//
//        } catch (Exception e) {
//            JOptionPane.showMessageDialog(null, e);
//        }
//    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        valorcurso = new javax.swing.JFormattedTextField();
        jLabel16 = new javax.swing.JLabel();
        codigocurso = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        nomecurso = new javax.swing.JTextField();
        totalhoras = new javax.swing.JFormattedTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        quantidademodulos = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        datacadastrocurso = new javax.swing.JFormattedTextField();
        jLabel18 = new javax.swing.JLabel();
        btnAdicionarCurso = new javax.swing.JButton();
        dataterminocurso = new javax.swing.JFormattedTextField();
        jLabel19 = new javax.swing.JLabel();
        btnEditar = new javax.swing.JButton();
        duracaocurso = new javax.swing.JComboBox<>();
        jLabel15 = new javax.swing.JLabel();
        jPanel17 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        pesqCurso = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        cmbCurso = new javax.swing.JComboBox<>();
        jScrollPane4 = new javax.swing.JScrollPane();
        tblCurso = new javax.swing.JTable();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Cadastrar Curso", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        try {
            valorcurso.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("R$ ####,##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jPanel3.add(valorcurso, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 120, 127, -1));

        jLabel16.setText("Quantidade de Módulo");
        jPanel3.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 100, -1, -1));

        codigocurso.setEditable(false);
        jPanel3.add(codigocurso, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 40, 127, -1));

        jLabel17.setText("Total de Horas");
        jPanel3.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 100, -1, -1));
        jPanel3.add(nomecurso, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 40, 281, -1));

        try {
            totalhoras.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("####   horas")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        totalhoras.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jPanel3.add(totalhoras, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 120, 127, -1));

        jLabel4.setText("Codigo");
        jPanel3.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, -1, -1));

        jLabel6.setText("Data de Cadastro");
        jPanel3.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 190, -1, -1));

        quantidademodulos.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1 Módulo", "2 Módulos", "3 Módulos", "4 Módulos", "5 Módulos", "6 Módulos", "7 Módulos", "8 Módulos", "9 Módulos", " " }));
        jPanel3.add(quantidademodulos, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 120, 136, -1));

        jLabel5.setText("Nome do curso");
        jPanel3.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 20, -1, -1));

        try {
            datacadastrocurso.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("## / ## / ####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        datacadastrocurso.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jPanel3.add(datacadastrocurso, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 210, 127, -1));

        jLabel18.setText("Data de Término");
        jPanel3.add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 190, -1, -1));

        btnAdicionarCurso.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/infox/icones/new-file 48.png"))); // NOI18N
        btnAdicionarCurso.setText("Adicionar");
        btnAdicionarCurso.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnAdicionarCurso.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btnAdicionarCurso.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnAdicionarCurso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAdicionarCursoActionPerformed(evt);
            }
        });
        jPanel3.add(btnAdicionarCurso, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 270, 88, -1));

        try {
            dataterminocurso.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("## / ## / ####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        dataterminocurso.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jPanel3.add(dataterminocurso, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 210, 127, -1));

        jLabel19.setText("Anos de Duração");
        jPanel3.add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 190, -1, -1));

        btnEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/infox/icones/Document.png"))); // NOI18N
        btnEditar.setText("Editar");
        btnEditar.setEnabled(false);
        btnEditar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnEditar.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btnEditar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });
        jPanel3.add(btnEditar, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 270, 88, -1));

        duracaocurso.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "1,5", "2", "2,5", "3", "3,5", "4", "4,5", "5" }));
        jPanel3.add(duracaocurso, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 210, 136, -1));

        jLabel15.setText("Valor do Curso");
        jPanel3.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 100, -1, -1));

        jPanel17.setBorder(javax.swing.BorderFactory.createTitledBorder("Pesquisar Curso"));

        jLabel2.setText("Localizar:");

        pesqCurso.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                pesqCursoKeyReleased(evt);
            }
        });

        jLabel21.setText("Por:");

        cmbCurso.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Nome", "Valor", "Modulos" }));
        cmbCurso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbCursoActionPerformed(evt);
            }
        });

        tblCurso.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Codigo", "Nome do Curso", "Valor", "Duração"
            }
        ));
        tblCurso.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblCursoMouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(tblCurso);

        javax.swing.GroupLayout jPanel17Layout = new javax.swing.GroupLayout(jPanel17);
        jPanel17.setLayout(jPanel17Layout);
        jPanel17Layout.setHorizontalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4)
                    .addGroup(jPanel17Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(5, 5, 5)
                        .addComponent(pesqCurso, javax.swing.GroupLayout.PREFERRED_SIZE, 431, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel21)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cmbCurso, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 14, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel17Layout.setVerticalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel17Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jLabel2))
                    .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cmbCurso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel21))
                    .addComponent(pesqCurso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(60, 60, 60)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 338, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 451, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(14, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel17, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Curso", jPanel1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 521, Short.MAX_VALUE)
                .addContainerGap())
        );

        setBounds(0, 0, 1229, 562);
    }// </editor-fold>//GEN-END:initComponents

    private void btnAdicionarCursoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAdicionarCursoActionPerformed
        adicionar_curso();
        pesquisar_curso();
    }//GEN-LAST:event_btnAdicionarCursoActionPerformed

    private void pesqCursoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pesqCursoKeyReleased
        pesquisar_curso();
    }//GEN-LAST:event_pesqCursoKeyReleased

    private void tblCursoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblCursoMouseClicked
        setar_campos();
        Validar_Curso();
    }//GEN-LAST:event_tblCursoMouseClicked

    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
        editar_curso();
        pesquisar_curso();
    }//GEN-LAST:event_btnEditarActionPerformed

    private void cmbCursoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbCursoActionPerformed
        pesquisar_curso();
    }//GEN-LAST:event_cmbCursoActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdicionarCurso;
    private javax.swing.JButton btnEditar;
    private javax.swing.JComboBox<String> cmbCurso;
    private javax.swing.JTextField codigocurso;
    private javax.swing.JFormattedTextField datacadastrocurso;
    private javax.swing.JFormattedTextField dataterminocurso;
    private javax.swing.JComboBox<String> duracaocurso;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTextField nomecurso;
    private javax.swing.JTextField pesqCurso;
    private javax.swing.JComboBox<String> quantidademodulos;
    private javax.swing.JTable tblCurso;
    private javax.swing.JFormattedTextField totalhoras;
    private javax.swing.JFormattedTextField valorcurso;
    // End of variables declaration//GEN-END:variables
}
