/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.infox.telas;

import java.text.DateFormat;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

/**
 *
 * @author PC-27
 */
public class TelaPrincipal extends javax.swing.JFrame {

    /**
     * Creates new form TelaPrincipal
     */
    public TelaPrincipal() {
        initComponents();
        
        idUsuario.setVisible(false);
        perfilUsu.setVisible(false);
        nomeUsu.setVisible(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        Aluno = new javax.swing.JMenuItem();
        Professor = new javax.swing.JMenuItem();
        Turma = new javax.swing.JMenuItem();
        jPopupMenu2 = new javax.swing.JPopupMenu();
        Sair = new javax.swing.JMenuItem();
        Editar = new javax.swing.JMenuItem();
        Desktop = new javax.swing.JDesktopPane();
        notas = new javax.swing.JButton();
        disc = new javax.swing.JButton();
        turma = new javax.swing.JButton();
        biblioteca = new javax.swing.JButton();
        pesquisa = new javax.swing.JButton();
        curso = new javax.swing.JButton();
        prof = new javax.swing.JButton();
        aluno = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        lblUsuario = new javax.swing.JLabel();
        lblData = new javax.swing.JLabel();
        idUsuario = new javax.swing.JTextField();
        perfilUsu = new javax.swing.JTextField();
        nomeUsu = new javax.swing.JTextField();
        menu = new javax.swing.JMenuBar();
        arquivo = new javax.swing.JMenu();
        jMenuItem5 = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        cadastro = new javax.swing.JMenu();
        jMenuItem6 = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        menCadCli = new javax.swing.JMenuItem();
        menCodOs = new javax.swing.JMenuItem();
        menCodOs1 = new javax.swing.JMenuItem();
        menDisc = new javax.swing.JMenuItem();
        menCodOs3 = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        ferramentas = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenu4 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        menOpc = new javax.swing.JMenu();
        menOpcSair = new javax.swing.JMenuItem();
        menAju = new javax.swing.JMenu();
        menAjuSob = new javax.swing.JMenuItem();

        Aluno.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/infox/icones/graduated.png"))); // NOI18N
        Aluno.setText("Aluno");
        Aluno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AlunoActionPerformed(evt);
            }
        });
        jPopupMenu1.add(Aluno);

        Professor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/infox/icones/teacher.png"))); // NOI18N
        Professor.setText("Gerenciar Professor");
        Professor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ProfessorActionPerformed(evt);
            }
        });
        jPopupMenu1.add(Professor);

        Turma.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/infox/icones/group teacher.png"))); // NOI18N
        Turma.setText("Turma");
        Turma.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TurmaActionPerformed(evt);
            }
        });
        jPopupMenu1.add(Turma);

        Sair.setText("Sair");
        Sair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SairActionPerformed(evt);
            }
        });
        jPopupMenu2.add(Sair);

        Editar.setText("Editar");
        Editar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EditarActionPerformed(evt);
            }
        });
        jPopupMenu2.add(Editar);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Tela Principal");
        setExtendedState(6);
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                formMouseReleased(evt);
            }
        });
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowActivated(java.awt.event.WindowEvent evt) {
                formWindowActivated(evt);
            }
        });

        Desktop.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                DesktopMouseReleased(evt);
            }
        });

        javax.swing.GroupLayout DesktopLayout = new javax.swing.GroupLayout(Desktop);
        Desktop.setLayout(DesktopLayout);
        DesktopLayout.setHorizontalGroup(
            DesktopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1255, Short.MAX_VALUE)
        );
        DesktopLayout.setVerticalGroup(
            DesktopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 625, Short.MAX_VALUE)
        );

        notas.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Notas", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));
        notas.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        notas.setFocusable(false);
        notas.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        notas.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        notas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                notasActionPerformed(evt);
            }
        });

        disc.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Disciplina", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));
        disc.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        disc.setFocusable(false);
        disc.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        disc.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        disc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                discActionPerformed(evt);
            }
        });

        turma.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Turma", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));
        turma.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        turma.setFocusable(false);
        turma.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        turma.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        turma.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                turmaActionPerformed(evt);
            }
        });

        biblioteca.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Biblioteca", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));
        biblioteca.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        biblioteca.setFocusable(false);
        biblioteca.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        biblioteca.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        biblioteca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bibliotecaActionPerformed(evt);
            }
        });

        pesquisa.setBorder(javax.swing.BorderFactory.createTitledBorder("Pesquisa "));
        pesquisa.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        pesquisa.setFocusable(false);
        pesquisa.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        pesquisa.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        pesquisa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pesquisaActionPerformed(evt);
            }
        });

        curso.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Curso", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));
        curso.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        curso.setFocusable(false);
        curso.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        curso.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        curso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cursoActionPerformed(evt);
            }
        });

        prof.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Professor", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));
        prof.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        prof.setFocusable(false);
        prof.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        prof.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        prof.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                profActionPerformed(evt);
            }
        });

        aluno.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Aluno", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));
        aluno.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        aluno.setFocusable(false);
        aluno.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        aluno.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        aluno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                alunoActionPerformed(evt);
            }
        });

        jButton6.setText("Usuario");
        jButton6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton6MouseClicked(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jButton6MouseReleased(evt);
            }
        });

        lblUsuario.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblUsuario.setText("Nome do Usuario");

        lblData.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblData.setText("Data do Sistema");

        idUsuario.setEditable(false);

        menu.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        menu.setBorderPainted(false);

        arquivo.setText("Arquivo");

        jMenuItem5.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_D, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem5.setText("Dados da Instituicao");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        arquivo.add(jMenuItem5);
        arquivo.add(jSeparator1);

        menu.add(arquivo);

        cadastro.setText("Cadastros");
        cadastro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cadastroActionPerformed(evt);
            }
        });

        jMenuItem6.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_U, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem6.setText("Usuario");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        cadastro.add(jMenuItem6);
        cadastro.add(jSeparator2);

        menCadCli.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        menCadCli.setText("Aluno");
        menCadCli.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menCadCliActionPerformed(evt);
            }
        });
        cadastro.add(menCadCli);

        menCodOs.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        menCodOs.setText("Professor");
        menCodOs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menCodOsActionPerformed(evt);
            }
        });
        cadastro.add(menCodOs);

        menCodOs1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_T, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        menCodOs1.setText("Turma");
        menCodOs1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menCodOs1ActionPerformed(evt);
            }
        });
        cadastro.add(menCodOs1);

        menDisc.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_D, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        menDisc.setText("Disciplinas");
        menDisc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menDiscActionPerformed(evt);
            }
        });
        cadastro.add(menDisc);

        menCodOs3.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        menCodOs3.setText("Notas");
        menCodOs3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menCodOs3ActionPerformed(evt);
            }
        });
        cadastro.add(menCodOs3);

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem1.setText("Curso");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        cadastro.add(jMenuItem1);

        jMenuItem3.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_L, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem3.setText("Livro");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        cadastro.add(jMenuItem3);

        menu.add(cadastro);

        ferramentas.setText("Ferramentas");

        jMenuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem2.setText("Calculadora");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        ferramentas.add(jMenuItem2);

        menu.add(ferramentas);

        jMenu4.setText("Dados da Instituicao");
        jMenu4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenu4ActionPerformed(evt);
            }
        });
        menu.add(jMenu4);

        jMenu2.setText("Troca de Usuario");
        jMenu2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenu2ActionPerformed(evt);
            }
        });
        menu.add(jMenu2);

        menOpc.setText("Opções");

        menOpcSair.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F4, java.awt.event.InputEvent.ALT_MASK));
        menOpcSair.setText("Sair");
        menOpcSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menOpcSairActionPerformed(evt);
            }
        });
        menOpc.add(menOpcSair);

        menu.add(menOpc);

        menAju.setText("Ajuda");

        menAjuSob.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F1, java.awt.event.InputEvent.ALT_MASK));
        menAjuSob.setText("Sobre");
        menAjuSob.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menAjuSobActionPerformed(evt);
            }
        });
        menAju.add(menAjuSob);

        menu.add(menAju);

        setJMenuBar(menu);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Desktop)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblData, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(idUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(perfilUsu, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(nomeUsu, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(lblUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 239, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 62, Short.MAX_VALUE)
                .addComponent(aluno, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(prof, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(curso, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(notas, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(disc, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(turma, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(biblioteca, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(pesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(aluno, javax.swing.GroupLayout.DEFAULT_SIZE, 67, Short.MAX_VALUE)
                        .addComponent(prof, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(curso, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(notas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(disc, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(turma, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(biblioteca, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(pesquisa, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblData, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(idUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(perfilUsu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(nomeUsu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(Desktop))
        );

        setSize(new java.awt.Dimension(1271, 768));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowActivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowActivated
        Date data = new Date();
        DateFormat formatador = DateFormat.getDateInstance(DateFormat.SHORT);
        lblData.setText(formatador.format(data));
        
        
    }//GEN-LAST:event_formWindowActivated

    private void menOpcSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menOpcSairActionPerformed
        // exibe uma caixa de usuario   
        int sair = JOptionPane.showConfirmDialog(null, "Tem certeza de quedesja sair?", "Atenção", JOptionPane.YES_NO_OPTION);
        if (sair == JOptionPane.YES_OPTION) {
            System.exit(0);
        }
    }//GEN-LAST:event_menOpcSairActionPerformed

    private void menAjuSobActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menAjuSobActionPerformed
        // chamando a tela sobre
        TelaSobre sobre = new TelaSobre();
        sobre.setVisible(true);
    }//GEN-LAST:event_menAjuSobActionPerformed

    private void menCadCliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menCadCliActionPerformed
        Tela_Aluno aluno = new Tela_Aluno();
        aluno.setVisible(true);
        Desktop.add(aluno);
        aluno.setPosicao();
    }//GEN-LAST:event_menCadCliActionPerformed

    private void menCodOsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menCodOsActionPerformed
        Tela_Professor professor = new Tela_Professor();
        professor.setVisible(true);
        Desktop.add(professor);
        professor.setPosicao();
    }//GEN-LAST:event_menCodOsActionPerformed

    private void menCodOs1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menCodOs1ActionPerformed
        Tela_Turma turma = new Tela_Turma();
        turma.setVisible(true);
        Desktop.add(turma);
        turma.setPosicao();
    }//GEN-LAST:event_menCodOs1ActionPerformed

    private void menDiscActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menDiscActionPerformed
        Tela_Disciplina disc = new Tela_Disciplina();
        disc.setVisible(true);
    }//GEN-LAST:event_menDiscActionPerformed

    private void menCodOs3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menCodOs3ActionPerformed
        Tela_Notas notas = new Tela_Notas();
        notas.setVisible(true);
        Desktop.add(notas);
        notas.setPosicao();
    }//GEN-LAST:event_menCodOs3ActionPerformed

    private void alunoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_alunoActionPerformed
        Tela_Aluno aluno = new Tela_Aluno();
        aluno.setVisible(true);
        Desktop.add(aluno);
        aluno.setPosicao();
    }//GEN-LAST:event_alunoActionPerformed

    private void profActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_profActionPerformed
        Tela_Professor professor = new Tela_Professor();
        professor.setVisible(true);
        Desktop.add(professor);
        professor.setPosicao();
    }//GEN-LAST:event_profActionPerformed

    private void turmaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_turmaActionPerformed
        Tela_Turma turma = new Tela_Turma();
        turma.setVisible(true);
        Desktop.add(turma);
        turma.setPosicao();
    }//GEN-LAST:event_turmaActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        Dados_Insti insti = new Dados_Insti();
        insti.setVisible(true);
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void bibliotecaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bibliotecaActionPerformed
        Tela_Biblioteca biblioteca = new Tela_Biblioteca();
        biblioteca.setVisible(true);
        Desktop.add(biblioteca);
        biblioteca.setPosicao();
    }//GEN-LAST:event_bibliotecaActionPerformed

    private void pesquisaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pesquisaActionPerformed
        Pesquisa pesquisa = new Pesquisa();
        pesquisa.setVisible(true);
        Desktop.add(pesquisa);
        pesquisa.setPosicao();
    }//GEN-LAST:event_pesquisaActionPerformed

    private void cursoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cursoActionPerformed
        Tela_Curso turma = new Tela_Curso();
        turma.setVisible(true);
        Desktop.add(turma);
        turma.setPosicao();
    }//GEN-LAST:event_cursoActionPerformed

    private void DesktopMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_DesktopMouseReleased
        if (evt.isPopupTrigger()) {
            jPopupMenu1.show(this, evt.getX(), evt.getY());
        }
    }//GEN-LAST:event_DesktopMouseReleased

    private void formMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseReleased

    }//GEN-LAST:event_formMouseReleased

    private void ProfessorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ProfessorActionPerformed
        Tela_Professor professor = new Tela_Professor();
        professor.setVisible(true);
        Desktop.add(professor);
        professor.setPosicao();
    }//GEN-LAST:event_ProfessorActionPerformed

    private void notasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_notasActionPerformed
        Tela_Notas notas = new Tela_Notas();
        notas.setVisible(true);
        Desktop.add(notas);
        notas.setPosicao();
    }//GEN-LAST:event_notasActionPerformed

    private void TurmaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TurmaActionPerformed
        Tela_Curso turma = new Tela_Curso();
        turma.setVisible(true);
        Desktop.add(turma);
        turma.setPosicao();
    }//GEN-LAST:event_TurmaActionPerformed

    private void AlunoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AlunoActionPerformed
        Tela_Aluno aluno = new Tela_Aluno();
        aluno.setVisible(true);
        Desktop.add(aluno);
        aluno.setPosicao();
    }//GEN-LAST:event_AlunoActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        Cadastro_de_Usuario usu = new Cadastro_de_Usuario();
        usu.setVisible(true);
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void discActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_discActionPerformed
        Tela_Disciplina disc = new Tela_Disciplina();
        disc.setVisible(true);
    }//GEN-LAST:event_discActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        Tela_Calculadora calc = new Tela_Calculadora();
        calc.setVisible(true);
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenu4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenu4ActionPerformed
        Dados_Insti insti = new Dados_Insti();
        insti.setVisible(true);
    }//GEN-LAST:event_jMenu4ActionPerformed

    private void jMenu2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenu2ActionPerformed
        int confirma = JOptionPane.showConfirmDialog(null, "Tem certeza de que deseja trocar de usuario?"
                + " Todas as  alteracoes nao salvas serao perdidas ", "Atenção", JOptionPane.YES_NO_OPTION);
        if (confirma == JOptionPane.YES_OPTION) {
            TelaLogin login = new TelaLogin();

            login.setVisible(true);
            this.dispose();
        }
    }//GEN-LAST:event_jMenu2ActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        Tela_Curso turma = new Tela_Curso();
        turma.setVisible(true);
        Desktop.add(turma);
        turma.setPosicao();
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void cadastroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cadastroActionPerformed
        Tela_Turma turma = new Tela_Turma();
        turma.setVisible(true);
        Desktop.add(turma);
        turma.setPosicao();
    }//GEN-LAST:event_cadastroActionPerformed

    private void jButton6MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton6MouseReleased
        if (evt.isPopupTrigger()) {
            jPopupMenu2.show(this, evt.getX(), evt.getY());
        }
    }//GEN-LAST:event_jButton6MouseReleased

    private void jButton6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton6MouseClicked

    }//GEN-LAST:event_jButton6MouseClicked

    private void SairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SairActionPerformed
        int confirma = JOptionPane.showConfirmDialog(null, "Tem certeza de que deseja sair?"
                + " Todas as  alteracoes nao salvas serao perdidas ", "Atenção", JOptionPane.YES_NO_OPTION);
        if (confirma == JOptionPane.YES_OPTION) {
            TelaLogin login = new TelaLogin();

            login.setVisible(true);
            this.dispose();
        }
    }//GEN-LAST:event_SairActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        Tela_Biblioteca biblioteca = new Tela_Biblioteca();
        biblioteca.setVisible(true);
        Desktop.add(biblioteca);
        biblioteca.setPosicao();
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void EditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EditarActionPerformed
       Cadastro_de_Usuario usu = new Cadastro_de_Usuario();
       usu.setVisible(true);
       usu.txtPesqUsu.setEnabled(false);
       usu.selectperfilusu.setEnabled(false);
       usu.tblpesquisausu.setEnabled(false);


    }//GEN-LAST:event_EditarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        try {
            UIManager.setLookAndFeel("com.jtattoo.plaf.texture.TextureLookAndFeel");
        } catch (Exception e) {
        }
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TelaPrincipal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem Aluno;
    private javax.swing.JDesktopPane Desktop;
    private javax.swing.JMenuItem Editar;
    private javax.swing.JMenuItem Professor;
    private javax.swing.JMenuItem Sair;
    private javax.swing.JMenuItem Turma;
    public static javax.swing.JButton aluno;
    public static javax.swing.JMenu arquivo;
    public static javax.swing.JButton biblioteca;
    public static javax.swing.JMenu cadastro;
    public static javax.swing.JButton curso;
    public static javax.swing.JButton disc;
    public static javax.swing.JMenu ferramentas;
    public static javax.swing.JTextField idUsuario;
    private javax.swing.JButton jButton6;
    public static javax.swing.JMenu jMenu2;
    public static javax.swing.JMenu jMenu4;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JPopupMenu jPopupMenu2;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    public static javax.swing.JLabel lblData;
    public static javax.swing.JLabel lblUsuario;
    public static javax.swing.JMenu menAju;
    private javax.swing.JMenuItem menAjuSob;
    private javax.swing.JMenuItem menCadCli;
    private javax.swing.JMenuItem menCodOs;
    private javax.swing.JMenuItem menCodOs1;
    private javax.swing.JMenuItem menCodOs3;
    private javax.swing.JMenuItem menDisc;
    public static javax.swing.JMenu menOpc;
    private javax.swing.JMenuItem menOpcSair;
    private javax.swing.JMenuBar menu;
    public static javax.swing.JTextField nomeUsu;
    public static javax.swing.JButton notas;
    public static javax.swing.JTextField perfilUsu;
    public static javax.swing.JButton pesquisa;
    public static javax.swing.JButton prof;
    public static javax.swing.JButton turma;
    // End of variables declaration//GEN-END:variables
}
