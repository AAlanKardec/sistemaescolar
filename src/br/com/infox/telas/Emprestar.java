/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.infox.telas;

import br.com.infox.dal.ModoConexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author Wendrews DJE
 */
public class Emprestar extends javax.swing.JFrame {

    Connection conexao = null;
    // criando variaveis especias para a conexao com o banco
    // PreparedStantement e Resulted são frameworks do  pacote java.sql e serve para preparar e executar as instruçoes SQL
    PreparedStatement pst = null;
    ResultSet rs = null;

    public Emprestar() {
        initComponents();
        conexao = ModoConexao.conector();

        id.setVisible(false);

        situacao.setVisible(false);
        situacaoLivro.setVisible(false);

        cmb_Turma();
        setar();
        setar_cod();
    }

    private void cmb_Turma() {
        String sql = "select * from tbturma  order by nometurma";
        try {
            pst = conexao.prepareStatement(sql);

            rs = pst.executeQuery();

            while (rs.next()) {
                cmbTurma.addItem(rs.getString("nometurma"));
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    private void adicionar() {
        
        String sql = "insert into tbemprestimo (nomeLivro,idlivro,codbarras,periodo,turma,"
                + "sala,aluno,dataemprestimo,dataDevolucao, situacao) value(?,?,?,?,?,?,?,?,?,?)";
        try {
            pst = conexao.prepareStatement(sql);
            //pst.setString(1, codigolivro.getText());
            pst.setString(1, nomeLivro.getText());
            pst.setString(2, id.getText());
            pst.setString(3, codBarras.getText());
            pst.setString(4, periodo.getSelectedItem().toString());
            pst.setString(5, cmbTurma.getSelectedItem().toString());
            pst.setString(6, sala.getText());
            pst.setString(7, nomeAluno.getText());
            pst.setString(8, dataEmprestimo.getText());
            pst.setString(9, dataDevolucao.getText());
            pst.setString(10, situacaoLivro.getText());

            if ((nomeLivro.getText().isEmpty())
                    || (periodo.getSelectedItem().toString().isEmpty())
                    || (cmbTurma.getSelectedItem().toString().isEmpty())
                    || (sala.getText().isEmpty())
                    || (nomeAluno.getText().isEmpty())
                    || (dataEmprestimo.getText().isEmpty())) {

                JOptionPane.showMessageDialog(null, "Preencha todos os campos!");

            } else {
                int adicionado = pst.executeUpdate();
                if (adicionado > 0) {
                    JOptionPane.showMessageDialog(null, "Emprestimo realizado com sucesso");
                    nomeLivro.setText(null);
//                    periodo.setSelectedItem(null);
//                    cmbTurma.setSelectedItem(null);
                    sala.setText(null);
                    nomeAluno.setText(null);
                    dataEmprestimo.setText(null);
                    dataDevolucao.setText(null);
                    codBarras.setText(null);
                }
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    private void alterar() {

        String sql = "update tbemprestimo set situacao=? where id=?";
        try {
            pst = conexao.prepareStatement(sql);
            pst.setString(1, situacao.getText());
            pst.setString(2, id.getText());

            if (id.getText().isEmpty()) {

                JOptionPane.showMessageDialog(null, "Preencha todos os campos!");
            } else {
                int adicionado = pst.executeUpdate();
                if (adicionado > 0) {
//                    JOptionPane.showMessageDialog(null, "Operacao realizada com sucesso");

                }
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }

    }

    private void devolver() {
        String sql = "update tbbiblioteca set situacao=? where idlivro=?";
        try {
            pst = conexao.prepareStatement(sql);

            pst.setString(1, situacao.getText());
            pst.setString(2, id.getText());

            if ((id.getText().isEmpty())) {

                JOptionPane.showMessageDialog(null, "Preencha todos os campos!");

            } else {
                int adicionado = pst.executeUpdate();
                if (adicionado > 0) {

                    JOptionPane.showMessageDialog(null, "Livro devolvido com sucesso");
                    nomeLivro.setText(null);
//                    periodo.setSelectedItem(null);
//                    cmbTurma.setSelectedItem(null);
                    sala.setText(null);
                    nomeAluno.setText(null);
                    dataEmprestimo.setText(null);
                    dataDevolucao.setText(null);

                }
            }

        } catch (Exception e) {

            JOptionPane.showMessageDialog(null, e);
        }

    }

    private void emprestar() {
        String sql = "update tbbiblioteca set situacao=? where idlivro=?";
        try {
            pst = conexao.prepareStatement(sql);

            pst.setString(1, situacaoLivro.getText());
            pst.setString(2, id.getText());

            if ((id.getText().isEmpty())) {

                JOptionPane.showMessageDialog(null, "Preencha todos os campos!");

            } else {
                int adicionado = pst.executeUpdate();
                if (adicionado > 0) {
//                    JOptionPane.showMessageDialog(null, "Emprestimo realizado com sucesso!");

                    nomeLivro.setText(null);
//                    periodo.setSelectedItem(null);
//                    cmbTurma.setSelectedItem(null);
                    sala.setText(null);
                    nomeAluno.setText(null);
                    dataEmprestimo.setText(null);
                    dataDevolucao.setText(null);

                }
            }

        } catch (Exception e) {

            JOptionPane.showMessageDialog(null, e);
        }

    }

    public void setar() {
        id.setText(Tela_Biblioteca.idLivro.getText());
        situacao.setText(Tela_Biblioteca.situacaoLivro.getText());

    }

    public void setar_cod() {
        String sql = "select * from tbbiblioteca where idlivro=?";
        try {
            pst = conexao.prepareStatement(sql);
            pst.setString(1, id.getText());
            rs = pst.executeQuery();
            if (rs.next()) {

                nomeLivro.setText(rs.getString(3));
                codBarras.setText(rs.getString(2));

                // alinha abaixo se refere ao combobox
            } else {
                JOptionPane.showMessageDialog(null, "Livro não cadastrado");
                // as linhas abaixo limpam os campos

            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }

    }

    public void consultar() {
        String sql = "select * from tbemprestimo where id=?";

        try {
            pst = conexao.prepareStatement(sql);
            pst.setString(1, id.getText());
            rs = pst.executeQuery();

            if (rs.next() & (situacao.getText().equals("Emprestado"))) {
                JOptionPane.showMessageDialog(null, "Este livro ainda esta emprestado");

            } else {
                nomeLivro.setText(rs.getString(3));
                codBarras.setText(rs.getString(5));

            }

        } catch (Exception e) {

        }

    }

//    private void consultar() {
//        String sql = "select * from tbemprestimo where id=? and situacao=?";
//        try {
//            
//            pst = conexao.prepareStatement(sql);
//            pst.setString(1, id.getText());
//            pst.setString(2, situacaoLivro.getText());
//            
//            rs = pst.executeQuery();
//            
//            if (rs.next()) {
//                      
//                nomeLivro.setText(rs.getString(3));
//                periodo.setSelectedItem(rs.getString(4));
//                codBarras.setText(rs.getString(5));
//                cmbTurma.setSelectedItem(rs.getString(6));
//                sala.setText(rs.getString(7));
//                nomeAluno.setText(rs.getString(8));
//                dataEmprestimo.setText(rs.getString(9));
//                dataDevolucao.setText(rs.getString(10));
////              situacaoLivro.setText(rs.getString(11));
//
//                // alinha abaixo desabilita os botoes
//                nomeLivro.setEnabled(false);
//                periodo.setEnabled(false);
//                codBarras.setEnabled(false);
//                cmbTurma.setEnabled(false);
//                sala.setEnabled(false);
//                nomeAluno.setEnabled(false);
//                dataDevolucao.setEnabled(false);
//                dataEmprestimo.setEnabled(false);
//                
//                 
//                // a linha abaixo valida a situacao
//            } else {
//
//                setar_cod();
//
////                JOptionPane.showMessageDialog(null, "Usuario não cadastrado");
//                nomeLivro.setEnabled(true);
//                periodo.setEnabled(true);
//                codBarras.setEnabled(true);
//                cmbTurma.setEnabled(true);
//                sala.setEnabled(true);
//                nomeAluno.setEnabled(true);
//                dataDevolucao.setEnabled(true);
//                dataEmprestimo.setEnabled(true);
//                
//
//                // as linhas abaixo limpam os campos
////                nomeLivro.setText(null);
////                codBarras.setText(null);
////                periodo.setSelectedItem(null);
////                cmbTurma.setSelectedItem(null);
//                sala.setText(null);
//
//                dataEmprestimo.setText(null);
//                dataDevolucao.setText(null);
//
//            }
//
//        } catch (Exception e) {
//            JOptionPane.showMessageDialog(null, e);
//        }
//    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        id = new javax.swing.JTextField();
        nomeAluno = new javax.swing.JTextField();
        jlabel = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        codBarras = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        nomeLivro = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        btnDevolver = new javax.swing.JButton();
        cmbTurma = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        periodo = new javax.swing.JComboBox<>();
        situacao = new javax.swing.JTextField();
        jlabel1 = new javax.swing.JLabel();
        sala = new javax.swing.JTextField();
        situacaoLivro = new javax.swing.JTextField();
        dataDevolucao = new javax.swing.JTextField();
        dataEmprestimo = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Emprestar Livro");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowActivated(java.awt.event.WindowEvent evt) {
                formWindowActivated(evt);
            }
        });

        id.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                idKeyReleased(evt);
            }
        });

        jlabel.setText("Data de Emprestimo");

        jLabel1.setForeground(new java.awt.Color(255, 0, 0));
        jLabel1.setText("Titulo do Livro");

        codBarras.setEditable(false);
        codBarras.setEnabled(false);

        jLabel7.setText("Data de Devolucao");

        jLabel2.setText("Periodo");

        jLabel5.setForeground(new java.awt.Color(255, 0, 0));
        jLabel5.setText("Codigo de barras");

        nomeLivro.setEditable(false);
        nomeLivro.setEnabled(false);

        jLabel3.setText("Turma");

        btnDevolver.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/infox/icones/new-file 48.png"))); // NOI18N
        btnDevolver.setText("Emprestar");
        btnDevolver.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnDevolver.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btnDevolver.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnDevolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDevolverActionPerformed(evt);
            }
        });

        jLabel4.setText("Sala");

        periodo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Manha", "Tarde", "Noite" }));
        periodo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                periodoActionPerformed(evt);
            }
        });

        jlabel1.setText("Nome do Aluno");

        dataDevolucao.setEditable(false);
        dataDevolucao.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        dataEmprestimo.setEditable(false);
        dataEmprestimo.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(19, 19, 19)
                                .addComponent(id, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(situacao, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(102, 102, 102))
                            .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(154, 154, 154))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(nomeLivro, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)))))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(codBarras, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(situacaoLivro, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(157, 157, 157)
                        .addComponent(btnDevolver, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(nomeAluno)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGap(39, 39, 39)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jlabel, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(dataEmprestimo, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(62, 62, 62)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(dataDevolucao, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(periodo, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cmbTurma, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addComponent(sala)))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGap(3, 3, 3)
                                .addComponent(jlabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addGap(15, 15, 15))
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {dataDevolucao, dataEmprestimo});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(id, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(situacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(situacaoLivro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nomeLivro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(codBarras, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jlabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(nomeAluno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jLabel3)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(periodo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(cmbTurma, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addComponent(jLabel2))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(sala, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jlabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(dataEmprestimo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(dataDevolucao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(btnDevolver)
                .addContainerGap(19, Short.MAX_VALUE))
        );

        setSize(new java.awt.Dimension(424, 406));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void idKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_idKeyReleased

    }//GEN-LAST:event_idKeyReleased

    private void btnDevolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDevolverActionPerformed

        if (situacao.getText().equals("Disponivel")) {
            situacaoLivro.setText("Emprestado");
            adicionar();
            emprestar();
            alterar();
            Tela_Biblioteca.Atualizar.doClick();
            this.dispose();
            
//            Tela_Biblioteca bib = new Tela_Biblioteca();
//            bib.setVisible(true);
//            bib.pesquisar_emprestimo();
//            bib.pesquisar_usuario();

        } else {

            situacaoLivro.setText("Disponivel");
            devolver();
            alterar();
            Tela_Biblioteca.Atualizar.doClick();
            this.dispose();
            
//            Tela_Biblioteca bib = new Tela_Biblioteca();
//            bib.setVisible(true);
//            bib.pesquisar_emprestimo();
//            bib.pesquisar_usuario();

        }
        Tela_Biblioteca.Atualizar.doClick();
        
    }//GEN-LAST:event_btnDevolverActionPerformed

    private void periodoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_periodoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_periodoActionPerformed

    private void formWindowActivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowActivated
       Date data = new Date();
        DateFormat formatador = DateFormat.getDateInstance(DateFormat.SHORT);
        dataEmprestimo.setText(formatador.format(data));
//        dataDevolucao.setText(formatador.format(data));

    }//GEN-LAST:event_formWindowActivated

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Emprestar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Emprestar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Emprestar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Emprestar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Emprestar().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnDevolver;
    private javax.swing.JComboBox<String> cmbTurma;
    private javax.swing.JTextField codBarras;
    private javax.swing.JTextField dataDevolucao;
    private javax.swing.JTextField dataEmprestimo;
    public static javax.swing.JTextField id;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jlabel;
    private javax.swing.JLabel jlabel1;
    private javax.swing.JTextField nomeAluno;
    private javax.swing.JTextField nomeLivro;
    private javax.swing.JComboBox<String> periodo;
    private javax.swing.JTextField sala;
    public static javax.swing.JTextField situacao;
    private javax.swing.JTextField situacaoLivro;
    // End of variables declaration//GEN-END:variables
}
