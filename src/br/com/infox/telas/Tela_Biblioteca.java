/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.infox.telas;

import br.com.infox.dal.ModoConexao;

import java.awt.Dimension;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.JOptionPane;
import net.proteanit.sql.DbUtils;

/**
 *
 * @author Wendrews DJE
 */
public class Tela_Biblioteca extends javax.swing.JInternalFrame {

    Connection conexao = null;
    //criando variaveis especiais para conexao com o banco
    //PreparedStatement e ResultSet são frameworks do pacote java.sql
    //e serve para preparar e executar as isntruções SQL
    PreparedStatement pst = null;
    ResultSet rs = null;

    public Tela_Biblioteca() {
        initComponents();
        conexao = ModoConexao.conector();
        pesquisar_usuario();
//        pesquisar_emprestimo();
        pesquisar_cmb();

        idLivro.setVisible(false);
        situacaoLivro.setVisible(false);
        idEmp.setVisible(false);
        situacaoEmp.setVisible(false);
        Atualizar.setVisible(false);

    }

    public void pesquisar_usuario() {
        String sql = "select * from tbbiblioteca where nomelivro like ?";
        try {
            pst = conexao.prepareStatement(sql);
            //passando o conteudo da caixa de pesquisa para o ?
            //atenção ao "%" - continuação da string sql
            pst.setString(1, pesquisarLivro.getText() + "%");
            rs = pst.executeQuery();
            //a linha abaixo usa a biblioteca rs2zml.jar para preencher a tabela
            tblLivro.setModel(DbUtils.resultSetToTableModel(rs));
        } catch (Exception e) {
        }
    }

    public void setar_campos() {

        int setar = tblLivro.getSelectedRow();
        idLivro.setText(tblLivro.getModel().getValueAt(setar, 0).toString());
        situacaoLivro.setText(tblLivro.getModel().getValueAt(setar, 8).toString());
        //a linha abaixo desabilitar o botao adicionar
        // btnaddusu.setEnabled(false);
    }

    public void setar_campos1() {

        int setar = tblEmp.getSelectedRow();
        idEmp.setText(tblEmp.getModel().getValueAt(setar, 0).toString());

        //a linha abaixo desabilitar o botao adicionar
//        btnaddusu.setEnabled(false);
    }

    private void setar_disciplina() {

        String sql = "select * from tbbiblioteca where idlivro=? and situacao=? ";
        try {
            pst = conexao.prepareStatement(sql);
            pst.setString(1, idLivro.getText());
            pst.setString(2, situacaoLivro.getText());

            rs = pst.executeQuery();

            if (rs.next() & (situacaoLivro.getText().equals("Disponivel"))) {

                int confirma = JOptionPane.showConfirmDialog(null, "Este livro esta disponivel para emprestimo,"
                        + " deseja empresta-lo?", "Atenção", JOptionPane.YES_NO_OPTION);
                if (confirma == JOptionPane.YES_OPTION) {

                    Emprestar emp = new Emprestar();
                    emp.setVisible(true);
                    emp.id.setText(idLivro.getText());
                    emp.situacao.setText(situacaoLivro.getText());

                    idLivro.setText(null);
                    situacaoLivro.setText(null);

                } else {

                }

            } else {

                JOptionPane.showMessageDialog(null, "Este Livro esta emprestado");

            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    public void setPosicao() {
        Dimension d = this.getDesktopPane().getSize();
        this.setLocation((d.width - this.getSize().width) / 2, (d.height - this.getSize().height) / 2);

    }

    // As linhas abaixo sao referentes ao painel Pesquisar Emprestimo
    public void pesquisar_emprestimo() {
        String sql = "select * from tbemprestimo where nomelivro like ?";
        try {
            pst = conexao.prepareStatement(sql);
            //passando o conteudo da caixa de pesquisa para o ?
            //atenção ao "%" - continuação da string sql
            pst.setString(1, pesquisarEmp.getText() + "%");
            rs = pst.executeQuery();
            //a linha abaixo usa a biblioteca rs2zml.jar para preencher a tabela
            tblEmp.setModel(DbUtils.resultSetToTableModel(rs));
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    private void consultar() {
        int setar = tblEmp.getSelectedRow();
        idEmp.setText(tblEmp.getModel().getValueAt(setar, 0).toString());
        situacaoEmp.setText(tblEmp.getModel().getValueAt(setar, 10).toString());

        String sql = "select * from tbemprestimo where id=?";

        try {
            pst = conexao.prepareStatement(sql);
            pst.setString(1, idEmp.getText());
            rs = pst.executeQuery();

            if (rs.next() & (situacaoEmp.getText().equals("Emprestado"))) {

                int confirma = JOptionPane.showConfirmDialog(null, "Deseja fazer a devolucao "
                        + "deste livro ?", "Atenção", JOptionPane.YES_NO_OPTION);
                if (confirma == JOptionPane.YES_OPTION) {

                    Devolver_Livro devolver = new Devolver_Livro();
                    devolver.setVisible(true);
                    devolver.id.setText(idEmp.getText());
                    devolver.situacao.setText(situacaoEmp.getText());

                } else {

                }

            } else {
                JOptionPane.showMessageDialog(null, "Este Livro ja foi devolvido");
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }

    }

    private void pesquisar_cmb() {

        if (cmbEmprestimo.getSelectedItem().equals("Nome do Livro")) {
            String sql = "select * from tbemprestimo where nomelivro like ? order by nomelivro";
            try {
                pst = conexao.prepareStatement(sql);
                //passando o conteudo da caixa de pesquisa para o ?
                //atenção ao "%" - continuação da string sql
                pst.setString(1, pesquisarEmp.getText() + "%");
                rs = pst.executeQuery();
                //a linha abaixo usa a biblioteca rs2zml.jar para preencher a tabela
                tblEmp.setModel(DbUtils.resultSetToTableModel(rs));
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }

        } else if (cmbEmprestimo.getSelectedItem().equals("Nome do Aluno")) {
            String sql = "select * from tbemprestimo where aluno like ? order by aluno";
            try {
                pst = conexao.prepareStatement(sql);
                //passando o conteudo da caixa de pesquisa para o ?
                //atenção ao "%" - continuação da string sql
                pst.setString(1, pesquisarEmp.getText() + "%");
                rs = pst.executeQuery();
                //a linha abaixo usa a biblioteca rs2zml.jar para preencher a tabela
                tblEmp.setModel(DbUtils.resultSetToTableModel(rs));
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }

        } else if (cmbEmprestimo.getSelectedItem().equals("Codigo de Barras")) {
            String sql = "select * from tbemprestimo where codbarras like ? order by codbarras";
            try {
                pst = conexao.prepareStatement(sql);
                //passando o conteudo da caixa de pesquisa para o ?
                //atenção ao "%" - continuação da string sql
                pst.setString(1, pesquisarEmp.getText() + "%");
                rs = pst.executeQuery();
                //a linha abaixo usa a biblioteca rs2zml.jar para preencher a tabela
                tblEmp.setModel(DbUtils.resultSetToTableModel(rs));
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }

        } else if (cmbEmprestimo.getSelectedItem().equals("Situacao")) {
            String sql = "select * from tbemprestimo where situacao like ? order by situacao";
            try {
                pst = conexao.prepareStatement(sql);
                //passando o conteudo da caixa de pesquisa para o ?
                //atenção ao "%" - continuação da string sql
                pst.setString(1, pesquisarEmp.getText() + "%");
                rs = pst.executeQuery();
                //a linha abaixo usa a biblioteca rs2zml.jar para preencher a tabela
                tblEmp.setModel(DbUtils.resultSetToTableModel(rs));
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }

        } else if (cmbEmprestimo.getSelectedItem().equals("Periodo")) {
            String sql = "select * from tbemprestimo where periodo like ? order by periodo";
            try {
                pst = conexao.prepareStatement(sql);
                //passando o conteudo da caixa de pesquisa para o ?
                //atenção ao "%" - continuação da string sql
                pst.setString(1, pesquisarEmp.getText() + "%");
                rs = pst.executeQuery();
                //a linha abaixo usa a biblioteca rs2zml.jar para preencher a tabela
                tblEmp.setModel(DbUtils.resultSetToTableModel(rs));
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }

        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        pesquisarLivro = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblLivro = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        Atualizar = new javax.swing.JButton();
        idLivro = new javax.swing.JTextField();
        situacaoLivro = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        pesquisarEmp = new javax.swing.JTextField();
        cmbEmprestimo = new javax.swing.JComboBox<>();
        idEmp = new javax.swing.JTextField();
        situacaoEmp = new javax.swing.JTextField();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblEmp = new javax.swing.JTable();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setTitle("Bilioteca");
        setPreferredSize(new java.awt.Dimension(1199, 495));

        jLabel1.setText("Pesquisar:");

        pesquisarLivro.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                pesquisarLivroKeyReleased(evt);
            }
        });

        tblLivro.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblLivro.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblLivroMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblLivro);

        jButton1.setText("Adicionar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Editar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        Atualizar.setText("Atualizar");
        Atualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AtualizarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 881, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pesquisarLivro, javax.swing.GroupLayout.PREFERRED_SIZE, 264, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(112, 112, 112)
                        .addComponent(idLivro, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(92, 92, 92)
                        .addComponent(situacaoLivro, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(36, 36, 36)
                .addComponent(jButton2)
                .addGap(44, 44, 44)
                .addComponent(Atualizar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {Atualizar, jButton1, jButton2});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(pesquisarLivro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(idLivro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(situacaoLivro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 359, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton2)
                    .addComponent(Atualizar))
                .addGap(25, 25, 25))
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {Atualizar, jButton1, jButton2});

        jTabbedPane1.addTab("Pesquisar Livro", jPanel1);

        jLabel2.setText("Pesquisar");

        pesquisarEmp.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                pesquisarEmpKeyReleased(evt);
            }
        });

        cmbEmprestimo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Situacao", "Nome do Livro", "Nome do Aluno", "Codigo de Barras", "Periodo" }));
        cmbEmprestimo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbEmprestimoActionPerformed(evt);
            }
        });

        tblEmp.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblEmp.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblEmpMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(tblEmp);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pesquisarEmp, javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(57, 57, 57)
                        .addComponent(cmbEmprestimo, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(222, 222, 222)
                        .addComponent(idEmp, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(82, 82, 82)
                        .addComponent(situacaoEmp, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 55, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(pesquisarEmp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbEmprestimo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(idEmp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(situacaoEmp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 464, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Historico", jPanel2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        cadastrar_Livro add = new cadastrar_Livro();
        add.setVisible(true);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void pesquisarLivroKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pesquisarLivroKeyReleased
        pesquisar_usuario();
    }//GEN-LAST:event_pesquisarLivroKeyReleased

    private void tblLivroMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblLivroMouseClicked
        setar_campos();
        setar_disciplina();
    }//GEN-LAST:event_tblLivroMouseClicked

    private void pesquisarEmpKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pesquisarEmpKeyReleased
        pesquisar_cmb();
    }//GEN-LAST:event_pesquisarEmpKeyReleased

    private void tblEmpMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblEmpMouseClicked
        consultar();
    }//GEN-LAST:event_tblEmpMouseClicked

    private void cmbEmprestimoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbEmprestimoActionPerformed
        pesquisar_cmb();

    }//GEN-LAST:event_cmbEmprestimoActionPerformed

    private void AtualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AtualizarActionPerformed
        pesquisar_cmb();
        pesquisar_usuario();
    }//GEN-LAST:event_AtualizarActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        if (idLivro.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Selecione um Livro");

        } else if (situacaoLivro.getText().equals("Emprestado")) {
            JOptionPane.showMessageDialog(null, "Efetue a devolucao do livro para fazer a edicao");
        } else {

            cadastrar_Livro cad = new cadastrar_Livro();
            cad.setVisible(true);

            cadastrar_Livro.btnadicionarlivro.setEnabled(false);
            cadastrar_Livro.btnalterarlivro.setEnabled(true);

        }

    }//GEN-LAST:event_jButton2ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JButton Atualizar;
    private javax.swing.JComboBox<String> cmbEmprestimo;
    public static javax.swing.JTextField idEmp;
    public static javax.swing.JTextField idLivro;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTextField pesquisarEmp;
    private javax.swing.JTextField pesquisarLivro;
    private javax.swing.JTextField situacaoEmp;
    public static javax.swing.JTextField situacaoLivro;
    private javax.swing.JTable tblEmp;
    private javax.swing.JTable tblLivro;
    // End of variables declaration//GEN-END:variables
}
