/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.infox.telas;

import br.com.infox.dal.ModoConexao;
import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import net.proteanit.sql.DbUtils;

/**
 *
 * @author Wendrews DJE
 */
public class Tela_Aluno extends javax.swing.JInternalFrame {

    Connection conexao = null;
    //criando variaveis especiais para conexao com o banco
    //PreparedStatement e ResultSet são frameworks do pacote java.sql
    //e serve para preparar e executar as isntruções SQL
    PreparedStatement pst = null;
    ResultSet rs = null;

    /**
     * Creates new form Tela_Aluno
     */
    public Tela_Aluno() {
        initComponents();
        conexao = ModoConexao.conector();
        alunoId.setVisible(false);
        Atualizar.setVisible(false);

        cmb_curso();
        pesquisar_aluno();

    }

    private void cmb_curso() {
        String sql = "select * from tbturma";
        try {
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            while (rs.next()) {
                cmbCurso.addItem(rs.getString("nomecurso"));
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    private void cmb_Turma() {
        String sql = "select * from tbturma where nomecurso=? order by nometurma";
        try {
            pst = conexao.prepareStatement(sql);
            pst.setString(1, cmbCurso.getSelectedItem().toString());

            rs = pst.executeQuery();

            while (rs.next()) {
                cmbTurma.addItem(rs.getString("nometurma"));
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    
    
    private void pesquisar_aluno() {

        if (cmbAluno.getSelectedItem().equals("Nome")) {
            String sql = "select idaluno as Codigo,nomealuno as Nome,telaluno as Telefone,"
                    + "emailaluno as EMail,cpfaluno as CPF, paialuno as Responsavel,"
                    + "celularpai as Contato, turmaaluno as Turma, cursoaluno as Curso "
                    + "from tbaluno where nomealuno like ? order by nomealuno";
            try {
                pst = conexao.prepareStatement(sql);
                //passando o conteudo da caixa de pesquisa para o ?
                //atenção ao "%" - continuação da string sql
                pst.setString(1, txtlocalizaraluno.getText() + "%");
                rs = pst.executeQuery();
                //a linha abaixo usa a biblioteca rs2zml.jar para preencher a tabela
                tblpesquisaraluno.setModel(DbUtils.resultSetToTableModel(rs));
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }

        } else if (cmbAluno.getSelectedItem().equals("CPF")) {
            String sql = "select idaluno as Codigo,nomealuno as Nome,telaluno as Telefone,"
                    + "emailaluno as EMail,cpfaluno as CPF, paialuno as Responsavel,"
                    + "celularpai as Contato, turmaaluno as Turma, cursoaluno as Curso "
                    + "from tbaluno where cpfaluno like ? order by cpfaluno ";
            try {
                pst = conexao.prepareStatement(sql);
                //passando o conteudo da caixa de pesquisa para o ?
                //atenção ao "%" - continuação da string sql
                pst.setString(1, txtlocalizaraluno.getText() + "%");
                rs = pst.executeQuery();
                //a linha abaixo usa a biblioteca rs2zml.jar para preencher a tabela
                tblpesquisaraluno.setModel(DbUtils.resultSetToTableModel(rs));
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }

        } else if (cmbAluno.getSelectedItem().equals("Codigo")) {
            String sql = "select idaluno as Codigo,nomealuno as Nome,telaluno as Telefone,"
                    + "emailaluno as EMail,cpfaluno as CPF, paialuno as Responsavel,"
                    + "celularpai as Contato, turmaaluno as Turma, cursoaluno as Curso"
                    + " from tbaluno where idaluno like ? order by idaluno ";
            try {
                pst = conexao.prepareStatement(sql);
                //passando o conteudo da caixa de pesquisa para o ?
                //atenção ao "%" - continuação da string sql
                pst.setString(1, txtlocalizaraluno.getText() + "%");
                rs = pst.executeQuery();
                //a linha abaixo usa a biblioteca rs2zml.jar para preencher a tabela
                tblpesquisaraluno.setModel(DbUtils.resultSetToTableModel(rs));
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }

        } else if (cmbAluno.getSelectedItem().equals("Turma")) {
            String sql = "select idaluno as Codigo,nomealuno as Nome,telaluno as Telefone,"
                    + "emailaluno as EMail,cpfaluno as CPF, paialuno as Responsavel,"
                    + "celularpai as Contato, turmaaluno as Turma, cursoaluno as Curso"
                    + " from tbaluno where turmaaluno like ? order by turmaaluno ";
            try {
                pst = conexao.prepareStatement(sql);
                //passando o conteudo da caixa de pesquisa para o ?
                //atenção ao "%" - continuação da string sql
                pst.setString(1, txtlocalizaraluno.getText() + "%");
                rs = pst.executeQuery();
                //a linha abaixo usa a biblioteca rs2zml.jar para preencher a tabela
                tblpesquisaraluno.setModel(DbUtils.resultSetToTableModel(rs));
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }

        }
        else if (cmbAluno.getSelectedItem().equals("Curso")) {
            String sql = "select idaluno as Codigo,nomealuno as Nome,telaluno as Telefone,"
                    + "emailaluno as EMail,cpfaluno as CPF, paialuno as Responsavel,"
                    + "celularpai as Contato, turmaaluno as Turma, cursoaluno as Curso"
                    + " from tbaluno where cursoaluno like ? order by cursoaluno ";
            try {
                pst = conexao.prepareStatement(sql);
                //passando o conteudo da caixa de pesquisa para o ?
                //atenção ao "%" - continuação da string sql
                pst.setString(1, txtlocalizaraluno.getText() + "%");
                rs = pst.executeQuery();
                //a linha abaixo usa a biblioteca rs2zml.jar para preencher a tabela
                tblpesquisaraluno.setModel(DbUtils.resultSetToTableModel(rs));
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }

        }

    }

    private void alterar_aluno() {

        String sql = "update tbaluno set dataaluno=?,cpfaluno=?,"
                + "nomealuno=?,datanasc=?,rgaluno=?,sexoaluno=?,"
                + "estcivilaluno=?,situacaoaluno=?,emailaluno=?,telaluno=?,"
                + "nacionalidadedaluno=?,cepaluno=?,cidadealuno=?,bairroaluno=?,"
                + "enderecoaluno=?,complementoaluno=?,numeroaluno=?,"
                + "cursoaluno=?, turmaaluno=?,paialuno=?,profissaopai=?,rgpai=?,emailpai=?,"
                + "celularpai=?,cpfpai=? where idaluno=?";

        try {
            pst = conexao.prepareStatement(sql);
            pst.setString(1, txtDatM1.getText());
            pst.setString(2, txtCpf1.getText());
            pst.setString(3, txtNome1.getText());
            pst.setString(4, txtNasc1.getText());
            pst.setString(5, txtRg1.getText());
            pst.setString(6, cmbSexo1.getSelectedItem().toString());
            pst.setString(7, cmbCivil1.getSelectedItem().toString());
            pst.setString(8, cmbStatus.getSelectedItem().toString());
            pst.setString(9, txtEmail1.getText());
            pst.setString(10, txtTel1.getText());
            pst.setString(11, txtNacionalidade1.getText());
            pst.setString(12, txtCep1.getText());
            pst.setString(13, cmbCidade1.getText());
            pst.setString(14, txtBairro1.getText());
            pst.setString(15, txtEndereco1.getText());
            pst.setString(16, txtComplemento1.getText());
            pst.setString(17, txtNumero1.getText());
            pst.setString(18, cmbCurso.getSelectedItem().toString());
            pst.setString(19, cmbTurma.getSelectedItem().toString());
            //            o metodo abaixo adiciona os campos do responsavel

            pst.setString(20, txtPaiAluno1.getText());
            pst.setString(21, txtProfissaoPai1.getText());
            pst.setString(22, txtRgPai1.getText());
            pst.setString(23, txtEmailPai1.getText());
            pst.setString(24, txtCelularPai1.getText());
            pst.setString(25, txtCpfPai1.getText());

            pst.setString(26, txtCod1.getText());

            if ((txtDatM1.getText().isEmpty()) || (txtNome1.getText().isEmpty())
                    || (cmbStatus.getSelectedItem().toString().isEmpty())
                    || (txtTel1.getText().isEmpty()) || (txtCep1.getText().isEmpty()) || (txtCep1.getText().isEmpty())
                    || (txtBairro1.getText().isEmpty()) || (txtEndereco1.getText().isEmpty())
                    || (txtNumero1.getText().isEmpty())) {
                JOptionPane.showMessageDialog(null, "Preencha os campos obrigatórios*");
            } else {
                int adicionado = pst.executeUpdate();
                if (adicionado > 0) {
                    JOptionPane.showMessageDialog(null, "Dados do aluno alterados com sucesso");

                    txtDatM1.setText(null);
                    txtCpf1.setText(null);
                    txtNome1.setText(null);
                    txtNasc1.setText(null);
                    txtRg1.setText(null);
                    cmbSexo1.setSelectedItem("Masculino");
                    cmbCivil1.setSelectedItem("Solteiro");
                    cmbStatus.setSelectedItem("Ativo");
                    txtEmail1.setText(null);
                    txtTel1.setText(null);
                    txtNacionalidade1.setText(null);
                    txtCep1.setText(null);
//                    cmbCidade1.setSelectedItem(null);
                    txtBairro1.setText(null);
                    txtEndereco1.setText(null);
                    txtComplemento1.setText(null);
                    txtNumero1.setText(null);
                    txtCod1.setText(null);

                }
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }

    }

    private void adicionar_aluno() {
        String sql = "insert into tbaluno ( dataaluno, "
                + "cpfaluno, nomealuno, datanasc, rgaluno, sexoaluno, estcivilaluno, "
                + "situacaoaluno, emailaluno, telaluno, nacionalidadedaluno, cepaluno, "
                + "cidadealuno, bairroaluno, enderecoaluno, complementoaluno,"
                + " numeroaluno, cursoaluno, turmaaluno, paialuno,profissaopai,rgpai,emailpai,"
                + "celularpai,cpfpai)values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        try {
            pst = conexao.prepareStatement(sql);

            pst.setString(1, txtDatM1.getText());
            pst.setString(2, txtCpf1.getText());
            pst.setString(3, txtNome1.getText());
            pst.setString(4, txtNasc1.getText());
            pst.setString(5, txtRg1.getText());
            pst.setString(6, cmbSexo1.getSelectedItem().toString());
            pst.setString(7, cmbCivil1.getSelectedItem().toString());
            pst.setString(8, cmbStatus.getSelectedItem().toString());
            pst.setString(9, txtEmail1.getText());
            pst.setString(10, txtTel1.getText());
            pst.setString(11, txtNacionalidade1.getText());
            pst.setString(12, txtCep1.getText());
            pst.setString(13, cmbCidade1.getText());
            pst.setString(14, txtBairro1.getText());
            pst.setString(15, txtEndereco1.getText());
            pst.setString(16, txtComplemento1.getText());
            pst.setString(17, txtNumero1.getText());
            pst.setString(18, cmbCurso.getSelectedItem().toString());
            pst.setString(19, cmbTurma.getSelectedItem().toString());
//            o metodo abaixo adiciona os campos do responsavel

            pst.setString(20, txtPaiAluno1.getText());
            pst.setString(21, txtProfissaoPai1.getText());
            pst.setString(22, txtRgPai1.getText());
            pst.setString(23, txtEmailPai1.getText());
            pst.setString(24, txtCelularPai1.getText());
            pst.setString(25, txtCpfPai1.getText());

            if ((txtDatM1.getText().isEmpty()) || (txtNome1.getText().isEmpty())
                    || (cmbStatus.getSelectedItem().toString().isEmpty())
                    || (txtTel1.getText().isEmpty()) || (txtCep1.getText().isEmpty()) || (txtCep1.getText().isEmpty())
                    || (txtBairro1.getText().isEmpty()) || (txtEndereco1.getText().isEmpty())
                    || (txtNumero1.getText().isEmpty()) || (cmbTurma.getSelectedItem().toString().equals("selecione"))) {
                JOptionPane.showMessageDialog(null, "Preencha os campos obrigatórios*");
            } else {
                int adicionado = pst.executeUpdate();
                if (adicionado > 0) {
                    JOptionPane.showMessageDialog(null, "Aluno adicionado com sucesso");

                    txtDatM1.setText(null);
                    txtCpf1.setText(null);
                    txtNome1.setText(null);
                    txtNasc1.setText(null);
                    txtRg1.setText(null);
                    cmbSexo1.setSelectedItem("Masculino");
                    cmbCivil1.setSelectedItem("Solteiro");
                    cmbStatus.setSelectedItem("Ativo");
                    txtEmail1.setText(null);
                    txtTel1.setText(null);
                    txtNacionalidade1.setText(null);
                    txtCep1.setText(null);
//                    cmbCidade1.setSelectedItem(null);
                    txtBairro1.setText(null);
                    txtEndereco1.setText(null);
                    txtComplemento1.setText(null);
                    txtNumero1.setText(null);

                    txtPaiAluno1.setText(null);
                    txtProfissaoPai1.setText(null);
                    txtRgPai1.setText(null);
                    txtEmailPai1.setText(null);
                    txtCelularPai1.setText(null);
                    txtCpfPai1.setText(null);

                }
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    private void setar_aluno() {

        int set = tblpesquisaraluno.getSelectedRow();
        alunoId.setText(tblpesquisaraluno.getModel().getValueAt(set, 0).toString());
//        txtCod1.setText(tblpesquisaraluno.getModel().getValueAt(set, 0).toString());
//        txtDatM1.setText(tblpesquisaraluno.getModel().getValueAt(set, 2).toString());
//        txtCpf1.setText(tblpesquisaraluno.getModel().getValueAt(set, 3).toString());
//        txtNome1.setText(tblpesquisaraluno.getModel().getValueAt(set, 4).toString());
//        txtNasc1.setText(tblpesquisaraluno.getModel().getValueAt(set, 5).toString());
//        txtRg1.setText(tblpesquisaraluno.getModel().getValueAt(set, 6).toString());
//        cmbSexo1.setSelectedItem(tblpesquisaraluno.getModel().getValueAt(set, 7).toString());
//        cmbCivil1.setSelectedItem(tblpesquisaraluno.getModel().getValueAt(set, 8).toString());
//        cmbStatus.setSelectedItem(tblpesquisaraluno.getModel().getValueAt(set, 9).toString());
//        txtEmail1.setText(tblpesquisaraluno.getModel().getValueAt(set, 10).toString());
//        txtTel1.setText(tblpesquisaraluno.getModel().getValueAt(set, 11).toString());
//        txtNacionalidade1.setText(tblpesquisaraluno.getModel().getValueAt(set, 12).toString());
//        txtCep1.setText(tblpesquisaraluno.getModel().getValueAt(set, 13).toString());
//        cmbCidade1.setText(tblpesquisaraluno.getModel().getValueAt(set, 14).toString());
//        txtBairro1.setText(tblpesquisaraluno.getModel().getValueAt(set, 15).toString());
//        txtEndereco1.setText(tblpesquisaraluno.getModel().getValueAt(set, 16).toString());
//        txtComplemento1.setText(tblpesquisaraluno.getModel().getValueAt(set, 17).toString());
//        txtNumero1.setText(tblpesquisaraluno.getModel().getValueAt(set, 18).toString());
//        cmbCurso.setSelectedItem(tblpesquisaraluno.getModel().getValueAt(set, 19).toString());
//        cmbTurma.setSelectedItem(tblpesquisaraluno.getModel().getValueAt(set, 20).toString());

//        btnAdcionar1.setEnabled(false);
    }

    private void adicionar_responsavel() {
        String sql = "isert into tbaluno (paialuno,profissaopai,rgpai,emailpai,celularpai,cpfpai)"
                + "values(?,?,?,?,?,?)";
        try {

            pst = conexao.prepareStatement(sql);
            pst.setString(1, txtPaiAluno1.getText());
            pst.setString(2, txtProfissaoPai1.getText());
            pst.setString(3, txtRgPai1.getText());
            pst.setString(4, txtEmailPai1.getText());
            pst.setString(5, txtCelularPai1.getText());
            pst.setString(6, txtCpfPai1.getText());

            int adicionado = pst.executeUpdate();

            if (adicionado > 0) {
                JOptionPane.showMessageDialog(null, "Responsáveis do aluno adicionado com sucesso");

            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }

    }

    public void setPosicao() {
        Dimension d = this.getDesktopPane().getSize();
        this.setLocation((d.width - this.getSize().width) / 2, (d.height - this.getSize().height) / 2);

    }

    private void consultar() {
        String sql = "select * from tbaluno where idaluno=?";
        try {
            pst = conexao.prepareStatement(sql);
            pst.setString(1, alunoId.getText());
            rs = pst.executeQuery();
            if (rs.next()) {

                txtDatM1.setText(rs.getString(3));
                txtCpf1.setText(rs.getString(4));
                txtNome1.setText(rs.getString(5));
                txtNasc1.setText(rs.getString(6));
                txtRg1.setText(rs.getString(7));
                cmbSexo1.setSelectedItem(rs.getString(8));
                cmbCivil1.setSelectedItem(rs.getString(9));
                cmbStatus.setSelectedItem(rs.getString(10));
                txtEmail1.setText(rs.getString(11));
                txtTel1.setText(rs.getString(12));
                txtNacionalidade1.setText(rs.getString(13));
                txtCep1.setText(rs.getString(14));
                cmbCidade1.setText(rs.getString(15));
                txtBairro1.setText(rs.getString(16));
                txtEndereco1.setText(rs.getString(17));
                txtComplemento1.setText(rs.getString(18));
                txtNumero1.setText(rs.getString(19));
                cmbCurso.setSelectedItem(rs.getString(20));
                cmbTurma.setSelectedItem(rs.getString(21));

                // alinha abaixo se refere ao combobox
            } else {
                JOptionPane.showMessageDialog(null, "Usuario não cadastrado");
                // as linhas abaixo limpam os campos

            }

        } catch (Exception e) {
//            JOptionPane.showMessageDialog(null, e);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblpesquisaraluno = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtlocalizaraluno = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        cmbAluno = new javax.swing.JComboBox<>();
        alunoId = new javax.swing.JTextField();
        Atualizar = new javax.swing.JButton();
        jPanel9 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jPanel10 = new javax.swing.JPanel();
        jPanel13 = new javax.swing.JPanel();
        jLabel65 = new javax.swing.JLabel();
        txtTel1 = new javax.swing.JFormattedTextField();
        jLabel67 = new javax.swing.JLabel();
        txtCod1 = new javax.swing.JTextField();
        txtNome1 = new javax.swing.JTextField();
        txtDatM1 = new javax.swing.JFormattedTextField();
        txtNasc1 = new javax.swing.JFormattedTextField();
        jLabel68 = new javax.swing.JLabel();
        txtCpf1 = new javax.swing.JFormattedTextField();
        txtEmail1 = new javax.swing.JTextField();
        jLabel69 = new javax.swing.JLabel();
        jLabel70 = new javax.swing.JLabel();
        jLabel71 = new javax.swing.JLabel();
        cmbCivil1 = new javax.swing.JComboBox<>();
        jLabel73 = new javax.swing.JLabel();
        jLabel74 = new javax.swing.JLabel();
        jLabel75 = new javax.swing.JLabel();
        cmbCurso = new javax.swing.JComboBox<>();
        jLabel21 = new javax.swing.JLabel();
        cmbTurma = new javax.swing.JComboBox<>();
        jLabel22 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        jLabel41 = new javax.swing.JLabel();
        txtEndereco1 = new javax.swing.JTextField();
        jLabel35 = new javax.swing.JLabel();
        txtBairro1 = new javax.swing.JFormattedTextField();
        jLabel25 = new javax.swing.JLabel();
        txtComplemento1 = new javax.swing.JTextField();
        jLabel30 = new javax.swing.JLabel();
        jLabel39 = new javax.swing.JLabel();
        txtNacionalidade1 = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel31 = new javax.swing.JLabel();
        txtNumero1 = new javax.swing.JTextField();
        txtRg1 = new javax.swing.JFormattedTextField();
        txtCep1 = new javax.swing.JFormattedTextField();
        cmbStatus = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        cmbSexo1 = new javax.swing.JComboBox<>();
        jLabel66 = new javax.swing.JLabel();
        cmbCidade1 = new javax.swing.JTextField();
        btnAdcionar1 = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        txtPaiAluno1 = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        txtEmailPai1 = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        txtProfissaoPai1 = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        txtCelularPai1 = new javax.swing.JFormattedTextField();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        txtRgPai1 = new javax.swing.JFormattedTextField();
        txtCpfPai1 = new javax.swing.JFormattedTextField();

        setClosable(true);
        setIconifiable(true);
        setResizable(true);
        setTitle("Aluno");
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/infox/icones/teacher.png"))); // NOI18N
        setPreferredSize(new java.awt.Dimension(0, 491));
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameOpened(evt);
            }
        });

        jPanel1.setPreferredSize(new java.awt.Dimension(929, 491));

        tblpesquisaraluno.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Codigo", "Nome do Aluno", "E-mail", "Telefone", "CPF", "CEP"
            }
        ));
        tblpesquisaraluno.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblpesquisaralunoMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblpesquisaraluno);

        jPanel4.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 204), 1, true));

        jLabel1.setText("Localizar:");

        txtlocalizaraluno.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtlocalizaralunoKeyReleased(evt);
            }
        });

        jLabel2.setText("Por:");

        cmbAluno.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Nome", "Turma", "CPF", "Codigo", "Curso" }));
        cmbAluno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbAlunoActionPerformed(evt);
            }
        });

        Atualizar.setText("Atualizar");
        Atualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AtualizarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(5, 5, 5)
                .addComponent(txtlocalizaraluno, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jLabel2)
                .addGap(10, 10, 10)
                .addComponent(cmbAluno, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(240, 240, 240)
                .addComponent(alunoId, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(81, 81, 81)
                .addComponent(Atualizar, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(65, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jLabel1))
                    .addComponent(txtlocalizaraluno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cmbAluno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(alunoId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(Atualizar)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 368, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Pesquisar Aluno", jPanel1);

        jPanel13.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));

        jLabel65.setText("Nascimento:*");

        try {
            txtTel1.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("( ## ) #### - ####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel67.setText("E-Mail:");

        txtCod1.setEnabled(false);

        try {
            txtDatM1.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("## /  ## / ####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtDatM1.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        try {
            txtNasc1.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("## / ## / ####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtNasc1.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jLabel68.setText("Telefone:*");

        try {
            txtCpf1.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###.###.### - ##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtCpf1.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jLabel69.setText("Estado Civil:");

        jLabel70.setText("Código:");

        jLabel71.setText("Nome do Aluno:*");

        cmbCivil1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Solteiro", "Casado", "Divorciado", "A espera de um milagre" }));
        cmbCivil1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbCivil1ActionPerformed(evt);
            }
        });

        jLabel73.setText("Data de Matrícula:*");

        jLabel74.setText("CPF:");

        jLabel75.setText("RG:");

        cmbCurso.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "selecione" }));
        cmbCurso.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbCursoItemStateChanged(evt);
            }
        });
        cmbCurso.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                cmbCursoMouseMoved(evt);
            }
        });
        cmbCurso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbCursoActionPerformed(evt);
            }
        });

        jLabel21.setForeground(new java.awt.Color(255, 0, 0));
        jLabel21.setText("Curso");

        jLabel22.setForeground(new java.awt.Color(255, 0, 0));
        jLabel22.setText("Turma");

        jLabel33.setText("CEP:*");

        jLabel41.setText("Endereço:*");

        jLabel35.setText("Cidade:");

        jLabel25.setText("Bairro:*");

        jLabel30.setText("Complemento:");

        jLabel39.setText("Nacionalidade:");

        jLabel31.setText("Número:*");

        try {
            txtRg1.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##.###.###")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtRg1.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        try {
            txtCep1.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("## . ### - ###")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtCep1.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        cmbStatus.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Ativo", "Inativo" }));

        jLabel3.setText("Status:*");

        cmbSexo1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Masculino", "Feminino" }));

        jLabel66.setText("Sexo:");

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel13Layout.createSequentialGroup()
                                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel13Layout.createSequentialGroup()
                                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel25)
                                            .addComponent(txtBairro1, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(18, 18, 18)
                                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel39)
                                            .addComponent(txtNacionalidade1, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addComponent(txtEndereco1, javax.swing.GroupLayout.PREFERRED_SIZE, 374, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel41, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel30)
                                    .addComponent(txtComplemento1, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createSequentialGroup()
                                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel13Layout.createSequentialGroup()
                                                .addComponent(jLabel31)
                                                .addGap(0, 0, Short.MAX_VALUE))
                                            .addComponent(txtNumero1))
                                        .addGap(18, 18, 18)
                                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addGroup(jPanel13Layout.createSequentialGroup()
                                                .addComponent(jLabel35)
                                                .addGap(89, 89, 89))
                                            .addComponent(cmbCidade1)))))
                            .addGroup(jPanel13Layout.createSequentialGroup()
                                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel71)
                                    .addComponent(txtNome1, javax.swing.GroupLayout.PREFERRED_SIZE, 374, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel13Layout.createSequentialGroup()
                                        .addGap(2, 2, 2)
                                        .addComponent(jLabel67))
                                    .addComponent(txtEmail1, javax.swing.GroupLayout.PREFERRED_SIZE, 374, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel13Layout.createSequentialGroup()
                                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel70)
                                            .addComponent(txtCod1, javax.swing.GroupLayout.PREFERRED_SIZE, 237, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(18, 18, 18)
                                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel73)
                                            .addComponent(txtDatM1))))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel13Layout.createSequentialGroup()
                                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(jLabel74, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(txtCpf1, javax.swing.GroupLayout.DEFAULT_SIZE, 125, Short.MAX_VALUE)
                                            .addComponent(jLabel75, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(txtRg1))
                                        .addGap(18, 18, 18)
                                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel68, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txtTel1, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel65, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txtNasc1, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(jPanel13Layout.createSequentialGroup()
                                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txtCep1, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel33))
                                        .addGap(18, 18, 18)
                                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(cmbSexo1, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel66)))))
                            .addGroup(jPanel13Layout.createSequentialGroup()
                                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel21)
                                    .addComponent(cmbCurso, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cmbTurma, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel69)
                                    .addComponent(cmbCivil1, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addComponent(cmbStatus, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                        .addGap(26, 26, 26))
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addComponent(jSeparator1)
                        .addContainerGap())))
        );

        jPanel13Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {cmbCivil1, cmbSexo1, jLabel65, jLabel66, jLabel68, jLabel69, jLabel74, jLabel75, txtCpf1, txtNasc1, txtTel1});

        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createSequentialGroup()
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel70)
                            .addComponent(jLabel73))
                        .addGap(6, 6, 6)
                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtCod1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtDatM1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addComponent(jLabel71)
                        .addGap(6, 6, 6)
                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtNome1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtRg1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addComponent(jLabel67)
                        .addGap(6, 6, 6)
                        .addComponent(txtEmail1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel13Layout.createSequentialGroup()
                                .addComponent(jLabel74)
                                .addGap(26, 26, 26))
                            .addGroup(jPanel13Layout.createSequentialGroup()
                                .addComponent(jLabel65)
                                .addGap(6, 6, 6)
                                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(txtNasc1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtCpf1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel75)
                            .addComponent(jLabel68))
                        .addGap(6, 6, 6)
                        .addComponent(txtTel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(jLabel66)
                        .addGap(6, 6, 6)
                        .addComponent(cmbSexo1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addComponent(jLabel33)
                        .addGap(26, 26, 26))
                    .addComponent(txtCep1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel13Layout.createSequentialGroup()
                            .addComponent(jLabel69)
                            .addGap(6, 6, 6)
                            .addComponent(cmbCivil1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel13Layout.createSequentialGroup()
                            .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel22)
                                .addComponent(jLabel21))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(cmbTurma, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(cmbCurso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(5, 5, 5)
                        .addComponent(cmbStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 11, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addComponent(jLabel35)
                        .addGap(26, 26, 26))
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addComponent(jLabel41)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtEndereco1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addComponent(jLabel31)
                        .addGap(6, 6, 6)
                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtNumero1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cmbCidade1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addComponent(jLabel30)
                        .addGap(6, 6, 6)
                        .addComponent(txtComplemento1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel13Layout.createSequentialGroup()
                            .addComponent(jLabel39)
                            .addGap(6, 6, 6)
                            .addComponent(txtNacionalidade1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel13Layout.createSequentialGroup()
                            .addComponent(jLabel25)
                            .addGap(6, 6, 6)
                            .addComponent(txtBairro1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(21, Short.MAX_VALUE))
        );

        btnAdcionar1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/infox/icones/new-file 48.png"))); // NOI18N
        btnAdcionar1.setText("Adicionar");
        btnAdcionar1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnAdcionar1.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btnAdcionar1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnAdcionar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAdcionar1ActionPerformed(evt);
            }
        });

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        jLabel15.setText("Nome do Pai");

        jLabel18.setText("E-Mail");

        jLabel16.setText("Profissao");

        jLabel17.setText("Celular");

        jLabel19.setText("RG");

        jLabel20.setText("CPF");

        try {
            txtRgPai1.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##.###.###")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtRgPai1.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        try {
            txtCpfPai1.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("## . ### - ###")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtCpfPai1.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel17)
                            .addComponent(txtProfissaoPai1, javax.swing.GroupLayout.DEFAULT_SIZE, 239, Short.MAX_VALUE)
                            .addComponent(txtCelularPai1))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtRgPai1)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGap(2, 2, 2)
                                        .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jLabel20))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(txtCpfPai1)))
                    .addComponent(txtEmailPai1)
                    .addComponent(txtPaiAluno1)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel15)
                            .addComponent(jLabel18))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(19, 19, 19))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jLabel15)
                .addGap(4, 4, 4)
                .addComponent(txtPaiAluno1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel18)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(txtEmailPai1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel16)
                        .addGap(4, 4, 4)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtProfissaoPai1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtRgPai1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 26, Short.MAX_VALUE)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(txtCelularPai1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtCpfPai1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel17)
                                .addGap(26, 26, 26)))
                        .addGap(34, 34, 34))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel19)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel20)
                        .addGap(60, 60, 60))))
        );

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addGap(175, 175, 175)
                        .addComponent(btnAdcionar1)
                        .addGap(146, 192, Short.MAX_VALUE))
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())))
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnAdcionar1))
                    .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jScrollPane2.setViewportView(jPanel10);

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 442, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Matricular Aluno", jPanel9);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 481, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        setBounds(0, 0, 1171, 510);
    }// </editor-fold>//GEN-END:initComponents

    private void formInternalFrameOpened(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameOpened

    }//GEN-LAST:event_formInternalFrameOpened

    private void txtlocalizaralunoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtlocalizaralunoKeyReleased
        pesquisar_aluno();
    }//GEN-LAST:event_txtlocalizaralunoKeyReleased

    private void cmbCivil1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbCivil1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbCivil1ActionPerformed

    private void btnAdcionar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAdcionar1ActionPerformed
        if ((cmbCurso.getSelectedItem().toString().equals("selecione")) || (cmbTurma.getSelectedItem().toString().isEmpty())) {
            JOptionPane.showMessageDialog(null, "Selecione um Curso e uma Turma");
        } else {
            adicionar_aluno();
        }


    }//GEN-LAST:event_btnAdcionar1ActionPerformed

    private void tblpesquisaralunoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblpesquisaralunoMouseClicked
        setar_aluno();
        int confirma = JOptionPane.showConfirmDialog(null, "Deseja alterar os dados do aluno selecionado ?", "Atenção", JOptionPane.YES_NO_OPTION);
        if (confirma == JOptionPane.YES_OPTION) {

            Tela_Editar_Aluno editar = new Tela_Editar_Aluno();
            editar.setVisible(true);

        }
    }//GEN-LAST:event_tblpesquisaralunoMouseClicked

    private void cmbCursoMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cmbCursoMouseMoved
//        cmbTurma.removeAllItems();
//        cmb_Turma();
    }//GEN-LAST:event_cmbCursoMouseMoved

    private void cmbCursoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbCursoItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbCursoItemStateChanged

    private void cmbCursoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbCursoActionPerformed
        if (cmbCurso.getSelectedItem().toString().contains("selecione")) {
            cmbTurma.removeAllItems();
        } else {
            cmbTurma.removeAllItems();
            cmb_Turma();
        }
    }//GEN-LAST:event_cmbCursoActionPerformed

    private void AtualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AtualizarActionPerformed
        pesquisar_aluno();
    }//GEN-LAST:event_AtualizarActionPerformed

    private void cmbAlunoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbAlunoActionPerformed
        pesquisar_aluno();
    }//GEN-LAST:event_cmbAlunoActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JButton Atualizar;
    public static javax.swing.JTextField alunoId;
    private javax.swing.JButton btnAdcionar1;
    private javax.swing.JComboBox<String> cmbAluno;
    private javax.swing.JTextField cmbCidade1;
    private javax.swing.JComboBox<String> cmbCivil1;
    private javax.swing.JComboBox<String> cmbCurso;
    private javax.swing.JComboBox<String> cmbSexo1;
    private javax.swing.JComboBox<String> cmbStatus;
    private javax.swing.JComboBox<String> cmbTurma;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel65;
    private javax.swing.JLabel jLabel66;
    private javax.swing.JLabel jLabel67;
    private javax.swing.JLabel jLabel68;
    private javax.swing.JLabel jLabel69;
    private javax.swing.JLabel jLabel70;
    private javax.swing.JLabel jLabel71;
    private javax.swing.JLabel jLabel73;
    private javax.swing.JLabel jLabel74;
    private javax.swing.JLabel jLabel75;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable tblpesquisaraluno;
    private javax.swing.JFormattedTextField txtBairro1;
    private javax.swing.JFormattedTextField txtCelularPai1;
    private javax.swing.JFormattedTextField txtCep1;
    private javax.swing.JTextField txtCod1;
    private javax.swing.JTextField txtComplemento1;
    private javax.swing.JFormattedTextField txtCpf1;
    private javax.swing.JFormattedTextField txtCpfPai1;
    private javax.swing.JFormattedTextField txtDatM1;
    private javax.swing.JTextField txtEmail1;
    private javax.swing.JTextField txtEmailPai1;
    private javax.swing.JTextField txtEndereco1;
    private javax.swing.JTextField txtNacionalidade1;
    private javax.swing.JFormattedTextField txtNasc1;
    private javax.swing.JTextField txtNome1;
    private javax.swing.JTextField txtNumero1;
    private javax.swing.JTextField txtPaiAluno1;
    private javax.swing.JTextField txtProfissaoPai1;
    private javax.swing.JFormattedTextField txtRg1;
    private javax.swing.JFormattedTextField txtRgPai1;
    private javax.swing.JFormattedTextField txtTel1;
    private javax.swing.JTextField txtlocalizaraluno;
    // End of variables declaration//GEN-END:variables
}
