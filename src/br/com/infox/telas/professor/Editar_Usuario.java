/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.infox.telas.professor;

import br.com.infox.dal.ModoConexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.JOptionPane;
import net.proteanit.sql.DbUtils;

/**
 *
 * @author Wendrews DJE
 */
public class Editar_Usuario extends javax.swing.JFrame {

     Connection conexao = null;
    //criando variaveis especiais para conexao com o banco
    //PreparedStatement e ResultSet são frameworks do pacote java.sql
    //e serve para preparar e executar as isntruções SQL
    PreparedStatement pst = null;
    ResultSet rs = null;

    
    public Editar_Usuario() {
        initComponents();
        
        conexao = ModoConexao.conector();
        txtcodusu.setText(TelaPrincipal2.idUsuario.getText());
        consultar();
//        idusuario();
        pesquisar_usuario();
        lblcodigousu.setVisible(false);
        txtcodusu.setVisible(false);
    }
    
//    private void idusuario() {
//        String perfil = TelaPrincipal2.perfilUsu.getText();
//        if (perfil.equals("Professor")) {
//            
//            txtPesqUsu.setEnabled(false);
//            btnAdd.setEnabled(false);
//            btnExc.setEnabled(false);
//            btnAlt.setEnabled(true);
//            
//            txtcodusu.setText(TelaPrincipal2.idUsuario.getText());
//            consultar();
//            
//        }  
//
////                txtcodusu.setText(TelaPrincipal_Professor.perfilUsu.getText());
//    }

    private void adicionar() {
        String sql = "insert into tbusuario (nomeusu,loginusu,senhausu,perfilusu)values(?,?,md5(?),?)";
//    String senha = txtsenhausu.getText();
        try {

//            MessageDigest md = MessageDigest.getInstance("SHA-256");
//            byte messageDigest[] = md.digest(senha.getBytes("UTF-8"));
//            
//            StringBuilder sb = new StringBuilder();
//            
//            for(byte b : messageDigest) {
//                
//                sb.append(String.format("%02X" , 0xFF & b));
//                
//                
//            }
//            String senhaHex = sb.toString();
            pst = conexao.prepareStatement(sql);

            pst.setString(1, txtNome.getText());
            pst.setString(2, txtloginusu.getText());
            pst.setString(3, txtsenhausu.getText());
            pst.setString(4, selectperfilusu.getSelectedItem().toString());
//            pst.setString(5, txtcodusu.getText());
            //validação dos campos obrigatorios
            if ((txtloginusu.getText().isEmpty()) || (txtsenhausu.getText().isEmpty())
                    || (selectperfilusu.getSelectedItem().toString().contains("Selecione uma Opcao"))) {
                JOptionPane.showMessageDialog(null, "Preencha todos os campos obrigatorios!");
            } else {

                //a linha abaixo atualiza a tabela usuario com os dados do formulario
                //a estrutura abaixo é usada para confirmar a inserção dos dados na tabela
                int adicionado = pst.executeUpdate();
                if (adicionado > 0) {
                    JOptionPane.showMessageDialog(null, "Usuario adicionado com Sucesso");

                    txtNome.setText(null);
                    txtloginusu.setText(null);
                    txtsenhausu.setText(null);
                    selectperfilusu.setSelectedItem("Selecione uma Opcao");

                }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    private void alterar() {
        String sql = "update tbusuario set nomeusu=?,loginusu=?,senhausu=md5(?),perfilusu=? where idusu=?";
        try {
            pst = conexao.prepareStatement(sql);

            pst.setString(1, txtNome.getText());
            pst.setString(2, txtloginusu.getText());
            pst.setString(3, txtsenhausu.getText());
            pst.setString(4, selectperfilusu.getSelectedItem().toString());
            pst.setString(5, txtcodusu.getText());
            //validação dos campos obrigatorios
            if ((txtloginusu.getText().isEmpty()) || (txtsenhausu.getText().isEmpty())) {
                JOptionPane.showMessageDialog(null, "Preencha todos os campos obrigatorios!");
            } else {

                //a linha abaixo atualiza a tabela usuario com os dados do formulario
                //a estrutura abaixo é usada para confirmar a inserção dos dados na tabela
                int adicionado = pst.executeUpdate();
                if (adicionado > 0) {
                    JOptionPane.showMessageDialog(null, "Usuario alterado com sucesso");

                    txtNome.setText(null);
                    txtcodusu.setText(null);
                    txtloginusu.setText(null);
                    txtsenhausu.setText(null);
                    selectperfilusu.setSelectedItem("Selecione uma Opcao");

                    btnAdd.setEnabled(true);
                    btnAlt.setEnabled(false);
                    btnExc.setEnabled(false);

                }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    private void consultar() {
        String sql = "select * from tbusuario where idusu=?";
        try {
            pst = conexao.prepareStatement(sql);
            pst.setString(1, txtcodusu.getText());
            rs = pst.executeQuery();
            if (rs.next()) {
                txtloginusu.setText(rs.getString(2));
                txtsenhausu.setText(rs.getString(3));
                selectperfilusu.setSelectedItem(rs.getString(4));
                txtNome.setText(rs.getString(5));

            } else {
                JOptionPane.showMessageDialog(null, "Usuario não cadastrado");
                // as linhas abaixo limpam os campos
                txtNome.setText(null);
                txtcodusu.setText(null);
                txtloginusu.setText(null);
                txtsenhausu.setText(null);
                selectperfilusu.setSelectedItem("Selecione uma Opcao");
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    private void pesquisar_usuario() {
        String sql = "select idusu as ID, nomeusu as Nome, loginusu as Login,"
                + " perfilusu as Perfil from tbusuario where loginusu like ?";
        try {
            pst = conexao.prepareStatement(sql);
            //passando o conteudo da caixa de pesquisa para o ?
            //atenção ao "%" - continuação da string sql
            pst.setString(1, txtPesqUsu.getText() + "%");
            rs = pst.executeQuery();
            //a linha abaixo usa a biblioteca rs2zml.jar para preencher a tabela
            tblpesquisausu.setModel(DbUtils.resultSetToTableModel(rs));
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    public void setar_campos() {

        int setar = tblpesquisausu.getSelectedRow();
        txtcodusu.setText(tblpesquisausu.getModel().getValueAt(setar, 0).toString());
//        txtloginusu.setText(tblpesquisausu.getModel().getValueAt(setar, 1).toString());
//        txtsenhausu.setText(tblpesquisausu.getModel().getValueAt(setar, 2).toString());
//        selectperfilusu.setSelectedItem(tblpesquisausu.getModel().getValueAt(setar, 3).toString());

        //a linha abaixo desabilitar o botao adicionar
//        btnaddusu.setEnabled(false);
    }

    private void remover() {
        int confirma = JOptionPane.showConfirmDialog(null, "Tem certeza de que deseja remover este Usuario?", "Atenção", JOptionPane.YES_NO_OPTION);
        if (confirma == JOptionPane.YES_OPTION) {
            String sql = "delete from tbusuario where idusu=?;";

            try {
                pst = conexao.prepareStatement(sql);
                pst.setString(1, txtcodusu.getText());

                int apagado = pst.executeUpdate();
                if (apagado > 0) {
                    JOptionPane.showMessageDialog(null, "Usuario excluido com sucesso");

                    txtNome.setText(null);
                    txtcodusu.setText(null);
                    txtloginusu.setText(null);
                    txtsenhausu.setText(null);
                    selectperfilusu.setSelectedItem(null);

                }

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }
        }
    }

    public void limpar() {

        txtNome.setText(null);
        txtcodusu.setText(null);
        txtloginusu.setText(null);
        txtsenhausu.setText(null);
        selectperfilusu.setSelectedItem(null);

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblpesquisausu = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        lblloginusu = new javax.swing.JLabel();
        lblsenhausu = new javax.swing.JLabel();
        lblrespostasecreta = new javax.swing.JLabel();
        lblperfilusu = new javax.swing.JLabel();
        txtloginusu = new javax.swing.JTextField();
        txtsenhausu = new javax.swing.JPasswordField();
        selectperfilusu = new javax.swing.JComboBox<>();
        btnAlt = new javax.swing.JButton();
        btnExc = new javax.swing.JButton();
        btnAdd = new javax.swing.JButton();
        txtcodusu = new javax.swing.JTextField();
        lblcodigousu = new javax.swing.JLabel();
        txtNome = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        txtPesqUsu = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Editar Usuario");

        tblpesquisausu.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Codigo", "Login", "Senha", "Perfil"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblpesquisausu.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblpesquisausuMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblpesquisausu);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Dados de Cadastro"));

        lblloginusu.setText("Login:");

        lblsenhausu.setText("Senha:");

        lblrespostasecreta.setText("Nome:");

        lblperfilusu.setText("Perfil:");

        txtloginusu.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtloginusuKeyReleased(evt);
            }
        });

        selectperfilusu.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Selecione uma Opcao", "Administrador", "Professor" }));
        selectperfilusu.setEnabled(false);

        btnAlt.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/infox/icones/Document.png"))); // NOI18N
        btnAlt.setText("Alterar");
        btnAlt.setEnabled(false);
        btnAlt.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnAlt.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btnAlt.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnAlt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAltActionPerformed(evt);
            }
        });

        btnExc.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/infox/icones/lixeira free- 48 x 48.png"))); // NOI18N
        btnExc.setText("Remover");
        btnExc.setEnabled(false);
        btnExc.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnExc.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btnExc.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnExc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcActionPerformed(evt);
            }
        });

        btnAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/infox/icones/new-file 48.png"))); // NOI18N
        btnAdd.setText("Adicionar");
        btnAdd.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnAdd.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btnAdd.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        txtcodusu.setEnabled(false);

        lblcodigousu.setText("Codigo");

        txtNome.setEnabled(false);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(lblcodigousu)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtcodusu, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(80, 80, 80)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblrespostasecreta)
                            .addComponent(lblloginusu))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtloginusu)
                            .addComponent(txtNome, javax.swing.GroupLayout.DEFAULT_SIZE, 188, Short.MAX_VALUE))
                        .addGap(58, 58, 58)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblsenhausu)
                            .addComponent(lblperfilusu))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(selectperfilusu, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtsenhausu, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(253, 253, 253)
                        .addComponent(btnAdd)
                        .addGap(27, 27, 27)
                        .addComponent(btnAlt)
                        .addGap(27, 27, 27)
                        .addComponent(btnExc)))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(selectperfilusu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblperfilusu)
                        .addComponent(txtNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblrespostasecreta))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtcodusu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblcodigousu))))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtsenhausu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblsenhausu)
                    .addComponent(lblloginusu)
                    .addComponent(txtloginusu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(36, 36, 36)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnAdd)
                    .addComponent(btnAlt)
                    .addComponent(btnExc))
                .addContainerGap(79, Short.MAX_VALUE))
        );

        jLabel1.setText("Pesquisar:");

        txtPesqUsu.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPesqUsuKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtPesqUsu, javax.swing.GroupLayout.PREFERRED_SIZE, 255, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtPesqUsu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 226, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Cadastro de Usuario", jPanel2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );

        setSize(new java.awt.Dimension(755, 643));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void tblpesquisausuMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblpesquisausuMouseClicked
        int confirma = JOptionPane.showConfirmDialog(null, "Deseja alterar este Usuario?", "Atenção", JOptionPane.YES_NO_OPTION);
        if (confirma == JOptionPane.YES_OPTION) {

            setar_campos();
            consultar();
            btnAdd.setEnabled(false);
            btnAlt.setEnabled(true);
            btnExc.setEnabled(true);

        } else {

        }
    }//GEN-LAST:event_tblpesquisausuMouseClicked

    private void txtloginusuKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtloginusuKeyReleased
        pesquisar_usuario();
    }//GEN-LAST:event_txtloginusuKeyReleased

    private void btnAltActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAltActionPerformed
        alterar();
        pesquisar_usuario();
        this.dispose();
    }//GEN-LAST:event_btnAltActionPerformed

    private void btnExcActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcActionPerformed
        remover();
        pesquisar_usuario();
//        this.dispose();
    }//GEN-LAST:event_btnExcActionPerformed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        adicionar();
        pesquisar_usuario();
        this.dispose();
    }//GEN-LAST:event_btnAddActionPerformed

    private void txtPesqUsuKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPesqUsuKeyReleased
        pesquisar_usuario();
    }//GEN-LAST:event_txtPesqUsuKeyReleased

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Editar_Usuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Editar_Usuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Editar_Usuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Editar_Usuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Editar_Usuario().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JButton btnAdd;
    public static javax.swing.JButton btnAlt;
    public static javax.swing.JButton btnExc;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel lblcodigousu;
    private javax.swing.JLabel lblloginusu;
    private javax.swing.JLabel lblperfilusu;
    private javax.swing.JLabel lblrespostasecreta;
    private javax.swing.JLabel lblsenhausu;
    private javax.swing.JComboBox<String> selectperfilusu;
    public static javax.swing.JTable tblpesquisausu;
    private javax.swing.JTextField txtNome;
    public static javax.swing.JTextField txtPesqUsu;
    private javax.swing.JTextField txtcodusu;
    private javax.swing.JTextField txtloginusu;
    private javax.swing.JPasswordField txtsenhausu;
    // End of variables declaration//GEN-END:variables
}
