package br.com.infox.telas;

import java.sql.*;
import br.com.infox.dal.ModoConexao;
import java.awt.Dimension;
import java.io.File;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import net.proteanit.sql.DbUtils;

/**
 *
 * @author Wendrews DJE
 */
public class Tela_Professor extends javax.swing.JInternalFrame {

    //Usando a variavel conexao do DAL
    Connection conexao = null;
    //criando variaveis especiais para conexao com o banco
    //PreparedStatement e ResultSet são frameworks do pacote java.sql
    //e serve para preparar e executar as isntruções SQL
    PreparedStatement pst = null;
    ResultSet rs = null;

    /**
     * Creates new form Tela_Professor
     */
    public Tela_Professor() {
        initComponents();
        conexao = ModoConexao.conector();

        tabela();

        profId.setVisible(false);
        Atualizar.setVisible(false);

    }

    //metodo para adicionar usuarios
    private void adicionar() {
        String sql = "insert into tbprofessor (nomeprof,enderecoprof, bairroprof, nascimentoprof,"
                + "cepprof,cidadeprof,estadoprof,emailprof,rgprof,cpfprof,telefoneprof,celularprof,"
                + "entradaprof,saidaprof,disciplinaprof,statusprof)values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        try {
            pst = conexao.prepareStatement(sql);
            //  pst.setString(1, codprofessor.getText());
            pst.setString(1, nomeprofessor.getText());
            pst.setString(2, endprofessor.getText());
            pst.setString(3, bairroprofessor.getText());
            pst.setString(4, datanascimentoprofessor.getText());
            pst.setString(5, cepprofessor.getText());
            pst.setString(6, cidadeprofessor.getText());
            pst.setString(7, estadoprofessor.getSelectedItem().toString());
            pst.setString(8, emailprofessor.getText());
            pst.setString(9, rgprofessor.getText());
            pst.setString(10, cpfprofessor.getText());
            pst.setString(11, telefoneprofessor.getText());
            pst.setString(12, celularprofessor.getText());
            pst.setString(13, entradaprofessor.getText());
            pst.setString(14, saidaprofessor.getText());
            pst.setString(15, disciplinaprofessor.getText());
            pst.setString(16, statusprofessor.getText());

            //validação dos campos obrigatorios
            if ((nomeprofessor.getText().isEmpty()) || (endprofessor.getText().isEmpty()) || (bairroprofessor.getText().isEmpty())
                    || (datanascimentoprofessor.getText().isEmpty()) || (cepprofessor.getText().isEmpty())
                    || (cidadeprofessor.getText().isEmpty()) || (emailprofessor.getText().isEmpty())
                    || (rgprofessor.getText().isEmpty()) || (cpfprofessor.getText().isEmpty())
                    || (telefoneprofessor.getText().isEmpty()) || (celularprofessor.getText().isEmpty())
                    || (entradaprofessor.getText().isEmpty()) || (saidaprofessor.getText().isEmpty())
                    || (disciplinaprofessor.getText().isEmpty()) || (statusprofessor.getText().isEmpty())) {
                JOptionPane.showMessageDialog(null, "Preencha todos os campos obrigatorios!");
            } else {

                //a linha abaixo atualiza a tabela usuario com os dados do formulario
                //a estrutura abaixo é usada para confirmar a inserção dos dados na tabela
                int adicionado = pst.executeUpdate();
                if (adicionado > 0) {
                    JOptionPane.showMessageDialog(null, "professor adicionado com Sucesso");
                    codprofessor.setText(null);
                    nomeprofessor.setText(null);
                    endprofessor.setText(null);
                    bairroprofessor.setText(null);
                    datanascimentoprofessor.setText(null);
                    cepprofessor.setText(null);
                    cidadeprofessor.setText(null);
                    estadoprofessor.setSelectedItem(null);
                    emailprofessor.setText(null);
                    rgprofessor.setText(null);
                    cpfprofessor.setText(null);
                    telefoneprofessor.setText(null);
                    celularprofessor.setText(null);
                    entradaprofessor.setText(null);
                    saidaprofessor.setText(null);
                    disciplinaprofessor.setText(null);
                    statusprofessor.setText(null);

                }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    //criando o metodo para alterar dados do usuario
    private void alterar() {
        String sql = "update tbprofessor set nomeprof=?,enderecoprof=?, bairroprof=?, nascimentoprof=?,"
                + "cepprof=?,cidadeprof=?,estadoprof=?,emailprof=?,rgprof=?,cpfprof=?,telefoneprof=?,celularprof=?,"
                + "entradaprof=?,saidaprof=?,disciplinaprof=?,statusprof=? where idprof=?";
        try {
            pst = conexao.prepareStatement(sql);
            pst.setString(1, nomeprofessor.getText());
            pst.setString(2, endprofessor.getText());
            pst.setString(3, bairroprofessor.getText());
            pst.setString(4, datanascimentoprofessor.getText());
            pst.setString(5, cepprofessor.getText());
            pst.setString(6, cidadeprofessor.getText());
            pst.setString(7, estadoprofessor.getSelectedItem().toString());
            pst.setString(8, emailprofessor.getText());
            pst.setString(9, rgprofessor.getText());
            pst.setString(10, cpfprofessor.getText());
            pst.setString(11, telefoneprofessor.getText());
            pst.setString(12, celularprofessor.getText());
            pst.setString(13, entradaprofessor.getText());
            pst.setString(14, saidaprofessor.getText());
            pst.setString(15, disciplinaprofessor.getText());
            pst.setString(16, statusprofessor.getText());
            pst.setString(17, codprofessor.getText());

            if ((nomeprofessor.getText().isEmpty()) || (endprofessor.getText().isEmpty()) || (bairroprofessor.getText().isEmpty())
                    || (datanascimentoprofessor.getText().isEmpty()) || (cepprofessor.getText().isEmpty())
                    || (cidadeprofessor.getText().isEmpty()) || (emailprofessor.getText().isEmpty())
                    || (rgprofessor.getText().isEmpty()) || (cpfprofessor.getText().isEmpty()) || (telefoneprofessor.getText().isEmpty())
                    || (celularprofessor.getText().isEmpty()) || (entradaprofessor.getText().isEmpty())
                    || (saidaprofessor.getText().isEmpty()) || (disciplinaprofessor.getText().isEmpty())
                    || (statusprofessor.getText().isEmpty())) {
                JOptionPane.showMessageDialog(null, "Preencha todos os campos obrigatorios");
            } else {

                //a linha abaixo atualiza a tabela usuario com os dados do formulario
                //a estrutura abaixo é usada para confirmar a alteração dos dados na tabela   
                int adicionado = pst.executeUpdate();
                if (adicionado > 0) {
                    JOptionPane.showMessageDialog(null, "Usuario alterado com Sucesso");
                    codprofessor.setText(null);
                    nomeprofessor.setText(null);
                    endprofessor.setText(null);
                    bairroprofessor.setText(null);
                    datanascimentoprofessor.setText(null);
                    cepprofessor.setText(null);
                    cidadeprofessor.setText(null);
                    estadoprofessor.setSelectedItem(null);
                    emailprofessor.setText(null);
                    rgprofessor.setText(null);
                    cpfprofessor.setText(null);
                    telefoneprofessor.setText(null);
                    celularprofessor.setText(null);
                    entradaprofessor.setText(null);
                    saidaprofessor.setText(null);
                    disciplinaprofessor.setText(null);
                    statusprofessor.setText(null);

                }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    //metodo para pesquisar clintes pelo nome com filtro
    private void tabela() {
        if (cmbProf.getSelectedItem().equals("Nome")) {

            String sql = "select idprof as Codigo, nomeprof as Nome,telefoneprof as Telefone,celularprof as Celular,"
                    + "emailprof as EMail from tbprofessor where nomeprof like ? order by nomeprof ";

            try {
                pst = conexao.prepareStatement(sql);
                //passando o conteudo da caixa de pesquisa para o ?
                //atenção ao "%" - continuação da string sql
                pst.setString(1, pesquisarprofessor.getText() + "%");
                rs = pst.executeQuery();
                //a linha abaixo usa a biblioteca rs2zml.jar para preencher a tabela
                tblprofessor.setModel(DbUtils.resultSetToTableModel(rs));
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }

        } else if (cmbProf.getSelectedItem().equals("Codigo")) {

            String sql = "select idprof as Codigo, nomeprof as Nome,telefoneprof as Telefone,celularprof as Celular,"
                    + "emailprof as EMail from tbprofessor where idprof like ? order by idprof ";

            try {
                pst = conexao.prepareStatement(sql);
                //passando o conteudo da caixa de pesquisa para o ?
                //atenção ao "%" - continuação da string sql
                pst.setString(1, pesquisarprofessor.getText() + "%");
                rs = pst.executeQuery();
                //a linha abaixo usa a biblioteca rs2zml.jar para preencher a tabela
                tblprofessor.setModel(DbUtils.resultSetToTableModel(rs));
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }

        } else if (cmbProf.getSelectedItem().equals("CPF")) {

            String sql = "select idprof as Codigo, nomeprof as Nome,telefoneprof as Telefone,celularprof as Celular,"
                    + "emailprof as EMail from tbprofessor where cpfprof like ? order by cpfprof ";

            try {
                pst = conexao.prepareStatement(sql);
                //passando o conteudo da caixa de pesquisa para o ?
                //atenção ao "%" - continuação da string sql
                pst.setString(1, pesquisarprofessor.getText() + "%");
                rs = pst.executeQuery();
                //a linha abaixo usa a biblioteca rs2zml.jar para preencher a tabela
                tblprofessor.setModel(DbUtils.resultSetToTableModel(rs));
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }

        }

    }

    //metodo para setar os campos do formulario com o conteudo da tabela
    public void setar_campos() {
        int setar = tblprofessor.getSelectedRow();
        profId.setText(tblprofessor.getModel().getValueAt(setar, 0).toString());
//        nomeprofessor.setText(tblprofessor.getModel().getValueAt(setar, 1).toString());
//        endprofessor.setText(tblprofessor.getModel().getValueAt(setar, 2).toString());
//        bairroprofessor.setText(tblprofessor.getModel().getValueAt(setar, 3).toString());
//        datanascimentoprofessor.setText(tblprofessor.getModel().getValueAt(setar, 4).toString());
//        cepprofessor.setText(tblprofessor.getModel().getValueAt(setar, 5).toString());
//        cidadeprofessor.setText(tblprofessor.getModel().getValueAt(setar, 6).toString());
//        estadoprofessor.setSelectedItem(tblprofessor.getModel().getValueAt(setar, 7).toString());
//        emailprofessor.setText(tblprofessor.getModel().getValueAt(setar, 8).toString());
//        rgprofessor.setText(tblprofessor.getModel().getValueAt(setar, 9).toString());
//        cpfprofessor.setText(tblprofessor.getModel().getValueAt(setar, 10).toString());
//        telefoneprofessor.setText(tblprofessor.getModel().getValueAt(setar, 11).toString());
//        celularprofessor.setText(tblprofessor.getModel().getValueAt(setar, 12).toString());
//        entradaprofessor.setText(tblprofessor.getModel().getValueAt(setar, 13).toString());
//        saidaprofessor.setText(tblprofessor.getModel().getValueAt(setar, 14).toString());
//        disciplinaprofessor.setText(tblprofessor.getModel().getValueAt(setar, 15).toString());
//        statusprofessor.setText(tblprofessor.getModel().getValueAt(setar, 16).toString());

        //a linha abaixo desabilitar o botao adicionar
        adicionarprof.setEnabled(false);
    }

    private void remover() {
        int confirma = JOptionPane.showConfirmDialog(null, "Tem certeza de que deseja remover este Professor?", "Atenção", JOptionPane.YES_NO_OPTION);
        if (confirma == JOptionPane.YES_OPTION) {
            String sql = "delete from tbprofessor where idprof=?;";

            try {
                pst = conexao.prepareStatement(sql);
                pst.setString(1, codprofessor.getText());

                int apagado = pst.executeUpdate();
                if (apagado > 0) {
                    JOptionPane.showMessageDialog(null, "Professor excluido com sucesso");

                    codprofessor.setText(null);
                    nomeprofessor.setText(null);
                    endprofessor.setText(null);
                    bairroprofessor.setText(null);
                    datanascimentoprofessor.setText(null);
                    cepprofessor.setText(null);
                    cidadeprofessor.setText(null);
                    estadoprofessor.setSelectedItem(null);
                    emailprofessor.setText(null);
                    rgprofessor.setText(null);
                    cpfprofessor.setText(null);
                    telefoneprofessor.setText(null);
                    celularprofessor.setText(null);
                    entradaprofessor.setText(null);
                    saidaprofessor.setText(null);
                    disciplinaprofessor.setText(null);
                    statusprofessor.setText(null);

                }

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }
        }
    }

    public void limpar() {

        codprofessor.setText(null);
        nomeprofessor.setText(null);
        endprofessor.setText(null);
        bairroprofessor.setText(null);
        datanascimentoprofessor.setText(null);
        cepprofessor.setText(null);
        cidadeprofessor.setText(null);
        estadoprofessor.setSelectedItem(null);
        emailprofessor.setText(null);
        rgprofessor.setText(null);
        cpfprofessor.setText(null);
        telefoneprofessor.setText(null);
        celularprofessor.setText(null);
        entradaprofessor.setText(null);
        saidaprofessor.setText(null);
        disciplinaprofessor.setText(null);
        statusprofessor.setText(null);

    }

    public void setPosicao() {
        Dimension d = this.getDesktopPane().getSize();
        this.setLocation((d.width - this.getSize().width) / 2, (d.height - this.getSize().height) / 2);

    }

    private void consultar() {
        String sql = "select * from tbprofessor where idprof=?";
        try {
            pst = conexao.prepareStatement(sql);
            pst.setString(1, profId.getText());
            rs = pst.executeQuery();
            if (rs.next()) {
                codprofessor.setText(rs.getString(1));
                nomeprofessor.setText(rs.getString(2));
                endprofessor.setText(rs.getString(3));
                datanascimentoprofessor.setText(rs.getString(5));
                bairroprofessor.setText(rs.getString(4));
                cepprofessor.setText(rs.getString(6));
                cidadeprofessor.setText(rs.getString(7));
                estadoprofessor.setSelectedItem(rs.getString(8));
                emailprofessor.setText(rs.getString(9));
                rgprofessor.setText(rs.getString(10));
                cpfprofessor.setText(rs.getString(11));
                telefoneprofessor.setText(rs.getString(12));
                celularprofessor.setText(rs.getString(13));
                entradaprofessor.setText(rs.getString(14));
                saidaprofessor.setText(rs.getString(15));
                disciplinaprofessor.setText(rs.getString(16));
                statusprofessor.setText(rs.getString(17));
                // alinha abaixo se refere ao combobox

            } else {
                JOptionPane.showMessageDialog(null, "Usuario não cadastrado");
                // as linhas abaixo limpam os campos

            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        lbllocalizar = new javax.swing.JLabel();
        pesquisarprofessor = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        cmbProf = new javax.swing.JComboBox<>();
        profId = new javax.swing.JTextField();
        Atualizar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblprofessor = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        lblenderecoprof = new javax.swing.JLabel();
        lblcodigoprof = new javax.swing.JLabel();
        lblnomeprof = new javax.swing.JLabel();
        lblnascprof = new javax.swing.JLabel();
        lblcepprof = new javax.swing.JLabel();
        lblestadoprof = new javax.swing.JLabel();
        endprofessor = new javax.swing.JTextField();
        codprofessor = new javax.swing.JTextField();
        datanascimentoprofessor = new javax.swing.JFormattedTextField();
        estadoprofessor = new javax.swing.JComboBox<>();
        nomeprofessor = new javax.swing.JTextField();
        bairroprofessor = new javax.swing.JTextField();
        lblbairroprof = new javax.swing.JLabel();
        cidadeprofessor = new javax.swing.JTextField();
        lblcidadeprof = new javax.swing.JLabel();
        emailprofessor = new javax.swing.JTextField();
        lblemailprof = new javax.swing.JLabel();
        lblrgprof = new javax.swing.JLabel();
        lblcpfprof = new javax.swing.JLabel();
        rgprofessor = new javax.swing.JTextField();
        lbltelprof = new javax.swing.JLabel();
        lblcelprof = new javax.swing.JLabel();
        lbldtaeprof = new javax.swing.JLabel();
        lbldtasprof = new javax.swing.JLabel();
        disciplinaprofessor = new javax.swing.JTextField();
        lbldicprof = new javax.swing.JLabel();
        adicionarprof = new javax.swing.JButton();
        lblloginprof = new javax.swing.JLabel();
        lblsenhaprof = new javax.swing.JLabel();
        lblstatusprof = new javax.swing.JLabel();
        statusprofessor = new javax.swing.JTextField();
        cepprofessor = new javax.swing.JFormattedTextField();
        celularprofessor = new javax.swing.JFormattedTextField();
        telefoneprofessor = new javax.swing.JFormattedTextField();
        cpfprofessor = new javax.swing.JFormattedTextField();
        saidaprofessor = new javax.swing.JFormattedTextField();
        entradaprofessor = new javax.swing.JFormattedTextField();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);

        jPanel4.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 204), 1, true));

        lbllocalizar.setText("Localizar:");

        pesquisarprofessor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                pesquisarprofessorKeyReleased(evt);
            }
        });

        jLabel2.setText("Por:");

        cmbProf.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Nome", "CPF", "Codigo" }));
        cmbProf.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbProfActionPerformed(evt);
            }
        });

        Atualizar.setText("Atualizar");
        Atualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AtualizarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbllocalizar)
                .addGap(5, 5, 5)
                .addComponent(pesquisarprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jLabel2)
                .addGap(10, 10, 10)
                .addComponent(cmbProf, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(261, 261, 261)
                .addComponent(profId, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(70, 70, 70)
                .addComponent(Atualizar, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(70, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(lbllocalizar))
                    .addComponent(pesquisarprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(cmbProf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(profId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(Atualizar)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        tblprofessor.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Codigo", "Nome do Professor", "E-mail", "Telefone", "CPF", "CEP"
            }
        ));
        tblprofessor.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblprofessorMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblprofessor);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1110, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 370, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Professor", jPanel1);

        lblenderecoprof.setText("Endereco:");

        lblcodigoprof.setText("Codigo");

        lblnomeprof.setText("Nome do professor:");

        lblnascprof.setText("Nascimento:");

        lblcepprof.setText("Cep:");

        lblestadoprof.setText("Estado:");

        codprofessor.setEditable(false);
        codprofessor.setEnabled(false);

        try {
            datanascimentoprofessor.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("## / ## / ####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        datanascimentoprofessor.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        estadoprofessor.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "MG", "SP", "AL", "DF" }));

        lblbairroprof.setText("Bairro:");

        lblcidadeprof.setText("Cidade:");

        lblemailprof.setText("E-mail:");

        lblrgprof.setText("RG:");

        lblcpfprof.setText("CPF:");

        lbltelprof.setText("Telefone Residencial");

        lblcelprof.setText("Celular:");

        lbldtaeprof.setText("Data de entrada:");

        lbldtasprof.setText("Data de saida:");

        lbldicprof.setText("Disciplina Lecionada:");

        adicionarprof.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/infox/icones/new-file 48.png"))); // NOI18N
        adicionarprof.setText("Adicionar");
        adicionarprof.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        adicionarprof.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        adicionarprof.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        adicionarprof.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                adicionarprofActionPerformed(evt);
            }
        });

        lblloginprof.setForeground(new java.awt.Color(255, 51, 51));
        lblloginprof.setText("(Login)");

        lblsenhaprof.setForeground(new java.awt.Color(255, 0, 0));
        lblsenhaprof.setText("(Senha)");

        lblstatusprof.setText("Status:");

        try {
            cepprofessor.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("## . ### - ###")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        cepprofessor.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        try {
            celularprofessor.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("( ## ) # #### - #### ")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        celularprofessor.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        try {
            cpfprofessor.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("### . ### . ### - ##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        cpfprofessor.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        try {
            saidaprofessor.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("## / ## / ####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        saidaprofessor.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        try {
            entradaprofessor.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("## / ## / ####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        entradaprofessor.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(rgprofessor, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE)
                                    .addComponent(lbldtaeprof, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(entradaprofessor))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cpfprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lbldtasprof, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(saidaprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(lbldicprof, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(telefoneprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(celularprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(lblstatusprof, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(disciplinaprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(statusprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(88, 88, 88)
                                .addComponent(adicionarprof))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(codprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(20, 20, 20)
                                .addComponent(nomeprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, 430, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(lblenderecoprof, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(390, 390, 390)
                                .addComponent(lblnascprof, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(endprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, 440, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(20, 20, 20)
                                .addComponent(datanascimentoprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(lblbairroprof, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(410, 410, 410)
                                .addComponent(lblcepprof))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(lblrgprof, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(90, 90, 90)
                                .addComponent(lblcpfprof, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(lblsenhaprof)
                                .addGap(72, 72, 72)
                                .addComponent(lbltelprof, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(20, 20, 20)
                                .addComponent(lblcelprof, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(bairroprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, 440, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(cepprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(lblcodigoprof, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(80, 80, 80)
                                .addComponent(lblnomeprof, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(lblloginprof, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 376, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(lblcidadeprof, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(400, 400, 400)
                                .addComponent(lblestadoprof))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(cidadeprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, 440, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(20, 20, 20)
                                .addComponent(estadoprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(lblemailprof, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(emailprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, 560, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(260, 545, Short.MAX_VALUE))))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {cpfprofessor, saidaprofessor});

        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblcodigoprof)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblnomeprof)
                        .addComponent(lblloginprof)))
                .addGap(6, 6, 6)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(codprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nomeprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblenderecoprof)
                    .addComponent(lblnascprof))
                .addGap(6, 6, 6)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(endprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(datanascimentoprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblbairroprof)
                    .addComponent(lblcepprof))
                .addGap(6, 6, 6)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bairroprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cepprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblcidadeprof)
                    .addComponent(lblestadoprof))
                .addGap(6, 6, 6)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cidadeprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(estadoprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addComponent(lblemailprof)
                .addGap(6, 6, 6)
                .addComponent(emailprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblrgprof)
                    .addComponent(lblcpfprof)
                    .addComponent(lblsenhaprof)
                    .addComponent(lbltelprof)
                    .addComponent(lblcelprof))
                .addGap(6, 6, 6)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(rgprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cpfprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(20, 20, 20)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lbldtaeprof)
                            .addComponent(lbldtasprof)))
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(adicionarprof)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(telefoneprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(jPanel2Layout.createSequentialGroup()
                                    .addComponent(celularprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(20, 20, 20)
                                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(lbldicprof)
                                        .addComponent(lblstatusprof))))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(disciplinaprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(statusprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(saidaprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(entradaprofessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(105, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Cadastro do Professor", jPanel2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jTabbedPane1)
                .addGap(8, 8, 8))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );

        setSize(new java.awt.Dimension(1154, 539));
    }// </editor-fold>//GEN-END:initComponents

    private void adicionarprofActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_adicionarprofActionPerformed
        adicionar();
    }//GEN-LAST:event_adicionarprofActionPerformed

    private void pesquisarprofessorKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pesquisarprofessorKeyReleased
        tabela();
    }//GEN-LAST:event_pesquisarprofessorKeyReleased

    private void tblprofessorMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblprofessorMouseClicked
        setar_campos();
        int confirma = JOptionPane.showConfirmDialog(null, "Deseja alterar os dados do Professor selecionado ?", "Atenção", JOptionPane.YES_NO_OPTION);
        if (confirma == JOptionPane.YES_OPTION) {

            Tela_Editar_Professor prof = new Tela_Editar_Professor();
            prof.setVisible(true);

        }

    }//GEN-LAST:event_tblprofessorMouseClicked

    private void AtualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AtualizarActionPerformed
        tabela();
    }//GEN-LAST:event_AtualizarActionPerformed

    private void cmbProfActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbProfActionPerformed
tabela();        // TODO add your handling code here:
    }//GEN-LAST:event_cmbProfActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JButton Atualizar;
    private javax.swing.JButton adicionarprof;
    private javax.swing.JTextField bairroprofessor;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JFormattedTextField celularprofessor;
    private javax.swing.JFormattedTextField cepprofessor;
    private javax.swing.JTextField cidadeprofessor;
    private javax.swing.JComboBox<String> cmbProf;
    private javax.swing.JTextField codprofessor;
    private javax.swing.JFormattedTextField cpfprofessor;
    private javax.swing.JFormattedTextField datanascimentoprofessor;
    private javax.swing.JTextField disciplinaprofessor;
    private javax.swing.JTextField emailprofessor;
    private javax.swing.JTextField endprofessor;
    private javax.swing.JFormattedTextField entradaprofessor;
    private javax.swing.JComboBox<String> estadoprofessor;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel lblbairroprof;
    private javax.swing.JLabel lblcelprof;
    private javax.swing.JLabel lblcepprof;
    private javax.swing.JLabel lblcidadeprof;
    private javax.swing.JLabel lblcodigoprof;
    private javax.swing.JLabel lblcpfprof;
    private javax.swing.JLabel lbldicprof;
    private javax.swing.JLabel lbldtaeprof;
    private javax.swing.JLabel lbldtasprof;
    private javax.swing.JLabel lblemailprof;
    private javax.swing.JLabel lblenderecoprof;
    private javax.swing.JLabel lblestadoprof;
    private javax.swing.JLabel lbllocalizar;
    private javax.swing.JLabel lblloginprof;
    private javax.swing.JLabel lblnascprof;
    private javax.swing.JLabel lblnomeprof;
    private javax.swing.JLabel lblrgprof;
    private javax.swing.JLabel lblsenhaprof;
    private javax.swing.JLabel lblstatusprof;
    private javax.swing.JLabel lbltelprof;
    private javax.swing.JTextField nomeprofessor;
    private javax.swing.JTextField pesquisarprofessor;
    public static javax.swing.JTextField profId;
    private javax.swing.JTextField rgprofessor;
    private javax.swing.JFormattedTextField saidaprofessor;
    private javax.swing.JTextField statusprofessor;
    private javax.swing.JTable tblprofessor;
    private javax.swing.JFormattedTextField telefoneprofessor;
    // End of variables declaration//GEN-END:variables
}
